$(function () {
	$('.navbar a.animate').click(function (e) {
		e.preventDefault();
		var href = $(this).attr('href');

		var top = $(href).offset().top;

		$('html, body').animate({
	        scrollTop: top
	      }, 800);
	});
})