/* ------------------------------------------------------------------------------
 *
 *  # Echarts - columns and waterfalls
 *
 *  Columns and waterfalls chart configurations
 *
 *  Version: 1.0
 *  Latest update: August 1, 2015
 *
 * ---------------------------------------------------------------------------- */

$(function () {

    var myChart = echarts.init(document.getElementById('echarts'));

    myChart.setOption({
        grid: {
            left: '1%',
            right: '1%',
            bottom: '1%',
            top: '5%',
            containLabel: true
        },
        xAxis: {
            data: [],
            axisTick: {
                alignWithLabel: true
            }
        },
        yAxis: {},
        series: [{
            itemStyle: {
                normal: {color:'#1ABB9C'}
            },
            name: 'Статистика',
            type: 'bar',
            data: [],
            label: {
                normal: {
                    show: true,
                    position: 'outside'
                }
            },
        }]
    });

    myChart.showLoading({
        color: '#1ABB9C',
    });

    $('.btn-stats .btn').click(function(event) {

        myChart.showLoading({
            color: '#1ABB9C',
        });

        var value = $(this).val();
        $(this).siblings().removeClass('btn-primary');
        $(this).addClass('btn-default');
        $(this).addClass('btn-primary');

        get(value);
    });
    
    get('day');

    function get(type) {
        $.ajax({
            type: "GET",
            url: 'stats/ajax',
            data: {type: type},
            success: function(data) {
                var array = $.parseJSON(data);
                table(array);
            }
        });
    }

    function table(data) {

        var labels = Object.keys(data);
        var data = Object.values(data);

        // specify chart configuration item and data
        var option = {
            xAxis: {
                data: labels
            },
            yAxis: {},
            series: [{
                data: data
            }]
        };

        // use configuration item and data specified to show chart
        myChart.hideLoading();
        myChart.setOption(option);
    }

    $(window).on('resize', function(){
        if(myChart != null && myChart != undefined){
            myChart.resize();
        }
    });
});