$(function() {

	var tabs = $('.tabs');
	var tab = $('.tab');
	var first_tab = $('.tab1');

	tab.first().addClass('opened');

	$('.tab').click(function(event) {
		var tab = $(this).attr('data-id');
		var active_tab = $(this);

		if(active_tab.hasClass('opened')) {
			$('.tab' + tab).hide();
			active_tab.removeClass('opened');
		} else {
			$('.tabs').hide();
			$('.tab' + tab).show();
			active_tab.addClass('opened');
		}
		
	});
})