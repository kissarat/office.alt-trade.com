{strip}
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>{if $title}{$title} | {/if}{$_cfg.Sys_SiteName}</title>

	<link rel="shortcut icon" href="/favicon.png" type="image/png">

	<!-- Bootstrap -->
	<link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- NProgress -->
	<link href="/vendors/nprogress/nprogress.css" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="/vendors/bootstrap-fileinput/css/fileinput.min.css">

	<!-- Custom Theme Style -->
	<link href="/build/css/custom.min.css" rel="stylesheet">
	<link rel="stylesheet" href="/css/main.css">

	<script src="/vendors/jquery/dist/jquery.min.js"></script>

</head>

<body class="nav-md">
    <div class="container body">
      <div class="main_container">
      	{include file='sidebar.tpl'}
      	{include file='nav.tpl'}
      	<div class="right_col" role="main">
      		{include file="stats/index.tpl"}
{/strip}