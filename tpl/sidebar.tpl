<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="https://alt-trade.com/" class="site_title">
      	<img src="/images/logocab.png" alt="">
      	<span>Alt Trade</span>
      </a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_pic">
      	{if $user_profile['aAvatar']}
			<img src="/upload/avatar/{$user_profile['aAvatar']}" alt="avatar" class="img-circle profile_img">
        {else}
            <img src="/images/user.png" alt="avatar" class="img-circle profile_img">
        {/if}
      </div>
      <div class="profile_info">
        <span>Welcome,</span>
        <h2>{$user.aName}</h2>
      </div>
      <div class="clearfix"></div>
    </div>
    <!-- /menu profile quick info -->

    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <ul class="nav side-menu">
        	<li class="{if $smarty.server.REQUEST_URI == '/cabinet'} active {/if}">
				<a href="/cabinet">
					<i class="fa fa-home fa-fw"></i>{$_TRANS['Cabinet']}
				</a>
			</li>
			{if $user.uLevel >= 90}
			<li>
				<a href="/admin">
					<i class="fa fa-lock fa-fw"></i>{$_TRANS['admin_panel']}
				</a>
			</li>
			{/if}
			<li class="{if $smarty.server.REQUEST_URI == '/deposits'} active {/if}">
				<a href="/deposits">
					<i class="fa fa-bank fa-fw"></i>{$_TRANS['my_investment']}
				</a>
			</li>
			<li class="{if $smarty.server.REQUEST_URI == '/operation?add=CASHIN'} active {/if}">
				<a href="/operation?add=CASHIN">
					<i class="fa fa-briefcase fa-fw"></i>{$_TRANS['refill_balance']}
				</a>
			</li>
			<li class="{if $smarty.server.REQUEST_URI == '/operation?add=CASHOUT'} active {/if}">
				<a href="/operation?add=CASHOUT">
					<i class="fa fa-refresh fa-fw"></i>{$_TRANS['withdrawal']}
				</a>
			</li>
			<li class="{if $smarty.server.REQUEST_URI == '/operations'} active {/if}">
				<a href="/operations">
					<i class="fa fa-calendar fa-fw"></i>{$_TRANS['operations']}
				</a>
			</li>
			<li class="{if $smarty.server.REQUEST_URI == '/refsys'} active {/if}">
				<a href="/refsys">
					<i class="fa fa-users fa-fw"></i>{$_TRANS['partner_program']}
				</a>
			</li>
			<li class="{if $smarty.server.REQUEST_URI == '/career'} active {/if}">
				<a href="/career">
					<i class="fa fa-signal fa-fw"></i>{$_TRANS['my_career']}
				</a>
			</li>
			<li class="{if $smarty.server.REQUEST_URI == '/promo'} active {/if}">
				<a href="/promo">
					<i class="fa fa-paper-plane-o fa-fw" aria-hidden="true"></i>{$_TRANS['advertising']}
				</a>
			</li>
			<li class="{if $smarty.server.REQUEST_URI == '/profile'} active {/if}">
				<a href="/profile">
					<i class="fa fa-user fa-fw"></i>{$_TRANS['Profile']}
				</a>
			</li>
			<li>
				<a href="https://alt-trade.com/faq/" target="_blank">
					<i class="fa fa-question fa-fw"></i>FAQ
				</a>
			</li>
			<li>
				<a href="/login?out">
					<i class="fa fa-sign-out fa-fw"></i>{$_TRANS['Logout']}
				</a>
			</li>
        </ul>
      </div>

    </div>
    <!-- /sidebar menu -->
  </div>
</div>
