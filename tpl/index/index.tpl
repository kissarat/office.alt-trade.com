{strip}
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Alt Trade</title>

    <link rel="shortcut icon" href="/favicon.png" type="image/png">
	
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery.bxslider.min.css">
    <link rel="stylesheet" href="css/lazyYT.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/main.css">

  </head>
  <body>
    <header>
        <canvas id="spiders" class="hidden-xs spiders"></canvas>
    	<div class="container">
            <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-navbar" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="logo pull-left">
                <a href="/"><img src="images/logo.png" alt=""></a>
            </div>
            </div>
    		<nav class="pull-right collapse navbar-collapse navbar-fixed-top" data-spy="affix" data-offset-top="50" id="collapse-navbar">
{*                     <ul class="lang">
                        <li><a href="interface?lang=ru"><img src="images/ru.png"></a></li>
                        <li><a href="interface?lang=en"><img src="images/en.png"></a></li>
                    </ul> *}
{*                     <div class="clearfix"></div> *}
                    <div class="container">
                        <ul class="navigation">
                        <li><a href="#section1">о компании</a></li>
                        <li><a href="#section2">инвесторам</a></li>
                        <li><a href="#section3">партнерам</a></li>
                        <li><a href="#section4">контакты</a></li>
                        <li><a href="cabinet">кабинет</a></li>
                    </ul> 
                    </div>        
    		</nav>
    		<div class="clearfix"></div>

    		<div class="page-header wow fadeIn">
    			<h1>зарабатывайте от 30% в месяц на крипторынке<br>
					<span>сделайте  вашу  мечту реальной</span>
				</h1>
    		</div>

    		<div class="login">
    			{include file='modal-register.tpl'}
                {include file='modal-login.tpl'}
    		</div>
    	</div>
    </header>

    <section class="about-us" id="section1">
    	<div class="container">
    		<div class="media about-article pull-right col-md-11 wow fadeInLeft">
                <div class="media-left media-middle">
                    <p>{$_TRANS['about_us']}</p>
                </div>
                <div class="media-body">
                    <p>{$_TRANS['about_us_text']}</p>
                </div>
            </div>
            <div class="clearfix"></div>
    		<div class="row about-company wow fadeInRight">
    			<div class="col-lg-12">
    				<div class="media">
    					<div class="media-left">
    						<img src="images/bulb.png" alt="bulb" class="pull-left">
    					</div>
    					<div class="media-body">
    						<h2>{$_TRANS['about_our_company']}</h2>
		    				<h3>{$_TRANS['about_our_company_title']}</h3>
		    				<p>{$_TRANS['about_our_company_text']}</p>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>

    <section class="garanties container">
    	<h1>Наши преимущества и гарантии</h1>
    	<div class="row">
    		<div class="col-md-4 text-center wow fadeInLeft">
    			<div class="image">
    				<img src="images/bank.png" alt="">
    			</div>
    			<h3>{$_TRANS['advantages']}</h3>
    			<p>{$_TRANS['advantages_text']}</p>
    		</div>
    		<div class="col-md-4 text-center wow fadeInDown">
    			<div class="image">
    				<img src="images/coin.png" alt="">
    			</div>
    			<h3>{$_TRANS['Profitability']}</h3>
    			<p>{$_TRANS['Profitability_text']}</p>
    		</div>
    		<div class="col-md-4 text-center wow fadeInRight">
    			<div class="image">
    				<img src="images/worldwide.png" alt="">
    			</div>
    			<h3>{$_TRANS['Availability']}</h3>
    			<p>{$_TRANS['Availability_text']}</p>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-md-4 text-center wow fadeInLeft">
    			<div class="image">
    				<img src="images/credit-card.png" alt="">
    			</div>
    			<h3>{$_TRANS['Operation_without_risk']}</h3>
    			<p>{$_TRANS['Operation_without_risk_text']}</p>
    		</div>
    		<div class="col-md-4 text-center wow fadeInUp">
    			<div class="image">
    				<img src="images/safebox.png" alt="">
    			</div>
    			<h3>{$_TRANS['Security']}</h3>
    			<p>{$_TRANS['Security_text']}</p>
    		</div>
    		<div class="col-md-4 text-center wow fadeInRight">
    			<div class="image">
    				<img src="images/phone-call.png" alt="">
    			</div>
    			<h3>{$_TRANS['Support']}</h3>
    			<p>{$_TRANS['Support_text']}</p>
    		</div>
    	</div>
    </section>

    <section class="quote">
        <div class="quote-text wow fadeIn">
            <span>“</span>
            <p>Инвестирование должно быть похоже на наблюдение за<br> высыхающей краской или растущей травой.</p>
            <span>“</span>
        </div>
        <div class="quote-line"></div>
        <h4>Пол Самуэльсон</h4>
    </section>

    <section class="about-us" id="section2">
        <div class="heading">
            <h1>Инвесторам</h1>
        </div>
        <div class="container">
            <div class="row about-company wow fadeInLeft">
                <div class="col-lg-12">
                    <div class="media">
                        <div class="media-left">
                            <img src="images/bulb.png" alt="bulb" class="pull-left">
                        </div>
                        <div class="media-body">
                            <h2>Инвестиционное предложение</h2>
                            <h3>ИНВЕСТИЦИИ С НАМИ – ЭТО ВЫГОДНО, ПРОСТО И НАДЕЖНО. ДЛЯ ВСЕХ ИНВЕСТОРОВ МЫ РАЗРАБОТАЛИ ОДИН ОПТИМАЛЬНЫЙ ИНВЕСТИЦИОННЫЙ ПЛАН С ДОХОДНОСТЬ ОТ 30% В МЕСЯЦ С ПЛАВАЮЩЕЙ ДОХОДНОСТЬЮ, В НЕ ЗАВИСИМОСТИ ОТ СУММЫ ВКЛАДА.</h3>
                            <p>Выплаты прибыли мы производим ежедневно на протяжении действия инвестиционного периода в 365 дней, за это время вы получите тело вклада плюс 260% чистого дохода. Начисление ежедневной прибыли происходит в автоматическом режиме, Вам просто необходимо зайти в свой личный кабинет и заказать вывод денежных средств на свой персональный кошелек в одной из электронных платежных систем с которой сотрудничает наша компания. Становитесь инвестором компании уже сегодня, и уже завтра Вы начнете получать свой стабильный доход от 30% в месяц от суммы вашей инвестиции.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="where-start">
        <h1>С чего начать?</h1>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 start-first wow flipInY">
                    <h3>Регистрация</h3>
                    <p>Нужно заполнить специальную форму с указанием логина, придумать пароль и повторно его ввести. Также потребуется указать ящик электронной почты, куда будут приходить уведомления и новости. В обязательном порядке нужно прописать данные платежной системы по умолчанию и ознакомиться с правилами компании.</p>
                    <a href="/registration">Начнем!</a>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 start-second wow flipInX">
                    <h3>ПОПОЛНЕНИЕ</h3>
                    <p>Свой депозит можно пополнить через электронные платежные системы Perfect Money, Advcash и Bitcoin. Начать вы можете даже с 10 долларов.</p>
                    <h4>от  <span>10</span> USD</h4>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 start-third wow flipInY">
                    <h3>Вывод <br> заработанных <br> денег</h3>
                    <p>После подачи заявки наш финансовый отдел обработает ваш запрос в течении 48 часов.</p>
                    <h4>Вывод: <span>в любое время</span></h4>
                </div>
            </div>
        </div>
    </section>

    <section class="quote">
        <div class="quote-text">
            <span>“</span>
            <p>Большие деньги не покупают или продают<br> большие деньги выжидают.</p>
            <span>“</span>
        </div>
        <div class="quote-line"></div>
        <h4>Джесси Ливермор</h4>
    </section>

    <section class="about-us" id="section3">
        <div class="heading">
            <h1>Партнерам</h1>
        </div>
        <div class="container">
            <div class="row about-company wow fadeInRight">
                <div class="col-lg-12">
                    <div class="media">
                        <div class="media-left">
                            <img src="images/bulb.png" alt="bulb" class="pull-left">
                        </div>
                        <div class="media-body">
                            <h2>Партнерская программа</h2>
                            <h3>НАША РЕФЕРАЛЬНАЯ СИСТЕМА ДАСТ ВОЗМОЖНОСТЬ ДЛЯ ЕЩЕ ОДНОГО ВЫСОКОГО И ПОСТОЯННО ЗАРАБОТКА.</h3>
                            <p>Создайте собственную партнерскую структуру на 2 уровня в глубину и Вы сможете получать до 15% от заработка рефералов Вашей структуры! За лично приглашенных партнеров вы получите 12% от их дохода, за рефералов второго уровня 3% от дохода. Если Вы умеете приглашать новых участников и готовы к активному заработку - наша реферальная программа это то, что Вам нужно! Просто поделитесь своей партнерской ссылкой с друзьями и знакомыми или разместите рекламу на доступных интернет-ресурсах и увеличивайте свой доход! Мы создали хорошие условия для старта, а дальше уровень Ваших доходов может быть неограниченным. Предприимчивость и активность, проявленная Вами в процессе приглашений в нашу программу новых участников, будет с лихвой вознаграждена нашей реферальной программой!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {if $list}
    {* <section class="news">
        <h1 class="text-center">Новости</h1>
        <div class="container">
            <div class="media about-article pull-right col-md-11">
                <div class="media-left media-middle">
                    <p>новости</p>
                </div>
                <div class="media-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis euismod id tellus a interdum. Phasellus suscipit nisl id imperdiet vulputate. Aliquam id feugiat ante. Nam lacinia finibus neque quis lobortis. Nulla ornare.</p>
                </div>
            </div>
            <div class="clearfix"></div>
            
            <div class="row">
                
                {foreach name=list from=$list key=id item=l}
                	<div class="col-lg-6 col-md-12 news-block">
                		<div class="news-img">
                			<div class="news-img-icon">
                				<img src="images/news-icon.png" alt="">
                			</div>
                			<img src="images/news-img-bg.png" alt="">
                		</div>
                		<div class="news-date">
                			<p>{$l.nTS}</p>
                		</div>
                		<div class="news-text">
            				<h3>{$l.nTopic}</h3>
            				<div class="announce">{$l.nAnnounce|nl2br}</div>
            				<div class="news-social">
                                <a href="{_link module='news/show' chpu=[$l.nID, $l.nTopic]}" class="news-more">{$_TRANS['read more']}</a>
            					<a href=""><img src="images/ico-vk.png" alt=""></a>
            					<a href=""><img src="images/ico-facebook.png" alt=""></a>
            					<a href=""><img src="images/ico-twitter.png" alt=""></a>
            					<a href=""><img src="images/ico-telegram.png" alt=""></a>
            					<a href=""><img src="images/ico-google.png" alt=""></a>
            				</div>
            			</div>
                	</div>
                {/foreach}

            </div>

        </div>
    </section> *}
    {/if}
    <section class="reviews">
        <h1 class="heading">Отзывы</h1>
    	<div class="container">
    		{* <div class="media about-article pull-right col-md-11">
                <div class="media-left media-middle">
                    <p>отзывы</p>
                </div>
                <div class="media-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis euismod id tellus a interdum. Phasellus suscipit nisl id imperdiet vulputate. Aliquam id feugiat ante. Nam lacinia finibus neque quis lobortis. Nulla ornare.</p>
                </div>
            </div> *}

            <ul id="reviews">
              <li>
                <h3>Андрей, инвестор</h3>
                <p>Я удовлетворен вложением своих активов в Alt-Trade, ведь это приносит мне хороший пассивный доход, я могу видеть всю необходимую информацию с регулярным обновлением данных. В общих чертах, мне импонирует этот проект, очень удобно - трейдеры зарабатывают я получаю прибыль!</p>
              </li>
              <li>
                <h3>Николай, активный инвестор</h3>
                <p>Alt-Trade предоставляет отличный сервис прибыльного вложения денег. Я действительно получаю доход от своего инвестирования, каждый день приходят выплаты мне и моим партнерам – полностью доверяю вам… Я бы поставил Alt-Trade 11 по 12-балльной системе. Процветания вам, ребята!</p>
              </li>
              <li>
                <h3>Михаил, инвестор</h3>
                <p>Alt-Trade – это мощная платформа зарубежного масштаба, которая приумножает мое вложения. Однозначно, она заслуживает пристального внимания со стороны инвесторов. На данный момент, я хотел бы очень высоко оценить тех поддержку проекта и поблагодарить трейдеров за такую прибыльность. Я неоднократно «нарывался» в своей практике на различные хайпы и фейковые проекты, но могу с уверенностью в 100 % сказать, что Alt-Trade – превосходная компания профессионалов</p>
              </li>
              <li>
                  <h3>Людмила, инвестор-новичок</h3>
                  <p>Довожу до ведома, что Alt-Trade – крутой инструмент, я сделала вложение в $ 500, а на следующий день уже получила первую выплату. В итоге, я обеспечила себя пассивным доходом, и что самое главное – вывожу денежку каждый день без никаких проблем. Теперь буду приобщать к этому делу всех своих знакомых и родственников. Спасибо команде Alt-Trade, вы – лучшие!»</p>
              </li>
            </ul>


            {* <div class="reviews-slider">
				<ul class="bxslider">
					<li>
                        <div class="lazyYT" data-youtube-id="ux9TF2E612c"></div>               
                    </li>
                    <li>
                        <div class="lazyYT" data-youtube-id="ux9TF2E612c"></div>               
                    </li>
                    <li>
                        <div class="lazyYT" data-youtube-id="ux9TF2E612c"></div>               
                    </li>
				</ul>
				<div class="control">
					<div class="prev"></div>
					<div class="pager"></div>
					<div class="next"></div>
				</div>
            </div> *}
    	</div>
    </section>

    <footer id="section4">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-6 footer-contacts">
{*     				<img src="images/logo.png" alt="">
    				<h4>Наши контакты</h4>
    				<a href=""><img src="images/mail-foot.png" alt="">&nbsp;vk.com/logo.business</a> *}
                    <div class="media about-article">
                        <div class="media-left media-middle">
                            <img src="images/logocab.png" alt="">
                        </div>
                        <div class="media-body">
                            <h4>Наши контакты</h4>
                            <a target="_blank" href="mailto:support@alt-trade.com"><img src="images/mail-foot.png" alt="">&nbsp;support@alt-trade.com</a>
                        </div>
                    </div>
    			</div>
    			<div class="col-md-6 footer-social">
    				<p>мы в социальных сетях</p>
    				<a href="https://vk.com/club146360963" target="_blank"><i class="fa fa-vk fa-fw" aria-hidden="true"></i></a>
    				<a href="https://www.facebook.com/alttradecom/" target="_blank"><i class="fa fa-facebook fa-fw" aria-hidden="true"></i></a>
    				<a href="https://www.youtube.com/channel/UCcrDsslcHuafsJuOPrNPk8w" target="_blank"><i class="fa fa-youtube fa-fw" aria-hidden="true"></i></a>
                    <a href="https://t.me/alttradechat" target="_blank"><i class="fa fa-telegram fa-fw" aria-hidden="true"></i></a>
    			</div>
    		</div>
    	</div>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://use.fontawesome.com/bdee4525e8.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.fitvids.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/lazyYT.js"></script>
    <script src="js/nodes.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/main.js"></script>
    <!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'cMr7zKJBSc';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->

<script src="/js/metrics.js"></script>
  </body>
</html>
{/strip}