{include file="header.tpl"}

<div class="container-fluid career">
	<div class="row">

	{foreach from=$profiles item=profile key=key}
	
	{if $profile['number'] == $user_career_id}
		<div class="col-md-6 col-xs-12 col-lg-4 active">
			<div class="well">
				<div class="col-xs-12 top">
					<div class="left col-xs-8 numbers">
						<h2><strong>{$_TRANS[$profile['name']]}</strong></h2>
						<p><strong>{$_TRANS['Conditions']}: </strong> </p>
						<ul class="list-unstyled">
							<li><i class="fa fa-opera" aria-hidden="true"></i> {$_TRANS['Structure turnover from']}: ${number_format($profile['amount'], 0, ' ', ' ')}</li>
							<li><i class="fa fa-money" aria-hidden="true"></i> {$_TRANS['Deposit from']}: ${number_format($profile['cashin'], 0, ' ', ' ')}</li>
						</ul>
					</div>
					<div class="right col-xs-4 text-center">
						<div class="logo">
							<i class="fa fa-star" aria-hidden="true"></i>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="text-center reward">
						<p><strong>{$_TRANS['Reward']}:</strong></p>
						<h1>
							<strong>${number_format($profile['award'], 0, ' ', ' ')}</strong>
						</h1>
					</div>
				</div>
				<div class="col-xs-12 bottom text-center">
					<div class="col-xs-10">
						<div class="progress">
							<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
								<span class="sr-only">100% Complete</span>
							</div>
						</div>
					</div>
					<div class="col-xs-2">
						<div class="icon-check">
							<i class="fa fa-check-circle-o" aria-hidden="true"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
	{else}
		<div class="col-md-6 col-xs-12 col-lg-4">
			<div class="well">
				<div class="col-xs-12 top">
					<div class="left col-xs-8 numbers">
						<h2><strong>{$_TRANS[$profile['name']]}</strong></h2>
						<p><strong>{$_TRANS['Conditions']}: </strong> </p>
						<ul class="list-unstyled">
							<li><i class="fa fa-opera" aria-hidden="true"></i> {$_TRANS['Structure turnover from']}: ${number_format($profile['amount'], 0, ' ', ' ')}</li>
							<li><i class="fa fa-money" aria-hidden="true"></i> {$_TRANS['Deposit from']}: ${number_format($profile['cashin'], 0, ' ', ' ')}</li>
						</ul>
					</div>
					<div class="right col-xs-4 text-center">
						<div class="logo">
							<i class="fa fa-star" aria-hidden="true"></i>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="text-center reward">
						<p><strong>{$_TRANS['Reward']}:</strong></p>
						<h1><strong>${number_format($profile['award'], 0, ' ', ' ')}</strong></h1>
					</div>
				</div>
				<div class="col-xs-12 bottom text-center">
						{if $profile['number'] < $user_career_id}
						<div class="col-xs-10">
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
									<span class="sr-only">100% Complete</span>
								</div>
							</div>
						</div class="col-xs-2">
							<div class="icon-check">
								<i class="fa fa-check-circle-o" aria-hidden="true"></i>
							</div>
						{/if}

						{if $profile['number'] == $user_career_id + 1}
							<div class="col-xs-10">
								<div class="progress">
									<div class="progress-bar" 
										role="progressbar" 
										aria-valuenow="{$result_percentage}" 
										aria-valuemin="0" 
										aria-valuemax="100"
										style="width: {$result_percentage}%"
									>
										<span class="sr-only">{$result_percentage}% Complete</span>
									</div>
								</div>
							</div>
							<div class="col-xs-2">
								<div class="icon-check">
									<i class="fa fa-bolt" aria-hidden="true"></i>
								</div>
							</div>
						{/if}
				</div>
			</div>
		</div>
	{/if}

	{/foreach}

</div>
</div>

{include file="footer.tpl"}