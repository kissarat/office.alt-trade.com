{strip}
{$states=[
	0=>$_TRANS['Non-active'],
	1=>$_TRANS['Active'],
	2=>$_TRANS['Blocked'],
	3=>$_TRANS['Disabled']
]}

<table cellspacing="0" cellpadding="0" border="0" class="table">
	<tr>
		<th>{$_TRANS['User']}</th>
		<th>Email</th>
		<th>{$_TRANS['Reg.date']}</th>
		<th>{$_TRANS['Deposit']} USD</th>
		<th>{$_TRANS['Deposit']} BTC</th>
		<th>{$_TRANS['Amount']}</th>
	</tr>
{foreach from=$refs key=i item=r}
	{if count($refs) > 1}
			<td id="tab" class="tab" data-id="{$i + 1}" colspan="6" align="center" bgcolor="#34495e" style="color: #fff">{$_TRANS['Level']} {$i + 1}{if $r.perc}: {$r.perc}%{/if}</td>
	{/if}
		{foreach from=$r.users key=j item=u}
			<tr class="tab{$i + 1} tabs">
				<td>{include file='_usericon.tpl' user=$u} {$u.uLogin}</td>
				<td>{$u.uMail}</td>
				<td>{date('d.m.Y', stampToTime($u.aCTS))}</td>
				<td align="left">{_z($u.uSumDepoUSD, 1)}</td>
				<td align="left">{_z($u.uSumDepoBTC, 2)}</td>
				<td align="left">{_z($u.ZRef, 1)}</td>
			</tr>
		{/foreach}
{/foreach}
</table>

{/strip}