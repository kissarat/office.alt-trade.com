{strip}
{include file='header.tpl'}

<div class="row balance">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>{$_TRANS['partners']}</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

{if _uid()}

	<div class="block_form">
		<form method="post" name="refsys_frm">

			{if $reflogin}
				<div class="block_form_el cfix">
				    <label for="refsys_frm_RefLogin">{$_TRANS['You invited by']}: <strong>{$reflogin}</strong></label><br>
				    <label for="">Email: <strong>{$refemail}</strong></label>
				</div>
			{/if}

			<div class="block_form_el cfix">
				<h2 for="refsys_frm_RefURL">{$_TRANS['Your referral link']}</h2>
			</div>

			<div id="ref-share-box" class="col-md-6" style="height: 57px;">
	        	<div class="form-group">
		            <form class="form-horizontal">
						<div class="col-sm-12 p-b-20">
							<div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-group fa-fw"></i></div>
								<input type="text" id="referalUrl" name="ref-link" value="{$refurl}" class="form-control" onclick="select()">
							</div>
						</div>
					</form>
				</div>
        	</div>

        	<div class="clearfix"></div>

			{if $refs}{include file="refsys/_refs.tpl"}{/if}

			{_getFormSecurity form='refsys_frm'}
		</form>
	</div>

	{if $_cfg.Account_RegMode == 3}

		<h2>{$_TRANS['Invites']}</h2>

	{/if}

{/if}

</div></div></div></div>

{include file='footer.tpl'}
{/strip}