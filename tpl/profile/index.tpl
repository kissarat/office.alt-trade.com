{include file="header.tpl"}

	<div class="x_panel">
		<div class="x_title">
			<h2>{$_TRANS['Profile']}</h2>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<ul class="nav nav-tabs bar_tabs">
				<li role="presentation" class="active"><a href="/profile">{$_TRANS['Profile']}</a></li>
				<li role="presentation"><a href="/account">{$_TRANS['Security']}</a></li>
				<li role="presentation"><a href="{_link module='account/change_pass'}">{$_TRANS['Change password']}</a></li>
				<li role="presentation"><a href="/balance/wallets">{$_TRANS['Payment details']}</a></li>
			</ul>
			<div class="col-md-6 col-sm-6 col-xs-12 profile_left">
				<div class="profile_img">
					<div id="crop-avatar">
						<!-- Current avatar -->
						{if $user_profile['aAvatar']}
							<img class="img-responsive avatar-view center-block" src="/upload/avatar/{$user_profile['aAvatar']}" alt="Avatar" title="avatar">
						{else}
							<img class="img-responsive avatar-view center-block" src="/images/user.png" alt="Avatar" title="Change the avatar">
						{/if}
					</div>
				</div>
				{if isset($error)}
					{$error}
				{/if}
				<br>
				<form method="post" action="/profile" enctype="multipart/form-data">
					<div class="form-group">
						<label>{$_TRANS['Choose photo']}:</label>
						{if $current_lang == 'ru'}
							<input id="logo-uploader-ru" type="file" name="logo">
						{else}
							<input id="logo-uploader-en" type="file" name="logo">
						{/if}
					</div>
				</form>
				<form action="/profile" method="post">
					<div class="form-group">
						<label>{$_TRANS['your_name']}</label>
						<input type="text" name="name" value="{$user_profile['aName']}" class="form-control">
					</div>

					<div class="form-group">
						<label>{$_TRANS['your_login']}</label>
						<input type="text" value="{$user.uLogin}" class="form-control" readonly>
					</div>

					<div class="form-group">
						<button class="btn btn-info" type="submit" name="profile">{$_TRANS['Save']}</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<script src="/js/profile.js"></script>

{include file="footer.tpl"}