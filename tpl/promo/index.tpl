{include file="header.tpl"}

	<div class="x_panel promo">
		<div class="x_title">
			<h2>{$_TRANS['advertising']}</h2>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<div class="col-md-6">
				<img src="/images/partner.jpg">
			</div>
			<div class="clearfix"></div>
			<a href="/lp?p={$user['uLogin']}" target="_blank" class="btn btn-primary">{$_TRANS['go']}</a>
		</div>
	</div>

<script src="/js/profile.js"></script>

{include file="footer.tpl"}