<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="ru-RU">
 <head>

  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <meta name="description" content="Зарабатывайте от 30% в месяц на крипторынке — сделайте Вашу мечту реальной. Зарабатывайте 17% партнерской комиссии с маркетингом Alt-Trade. Приумножайте деньги"/>
  <meta name="generator" content="2017.0.1.363"/>

  
  <link rel="shortcut icon" href="images/alt-trade--%d0%b7%d0%b0%d1%80%d0%b0%d0%b1%d0%be%d1%82%d0%be%d0%ba-%d0%bd%d0%b0-%d0%ba%d1%80%d0%b8%d0%bf%d1%82%d0%be%d0%b2%d0%b0%d0%bb%d1%8e%d1%82%d0%b5-favicon.ico?crc=286708266"/>
  <title>Alt-Trade | Заработок на криптовалюте</title>
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="css/site_global.css?crc=3945018967"/>
  <link rel="stylesheet" type="text/css" href="css/index.css?crc=455986357" id="pagesheet"/>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 </head>
 <body>

  <div class="clearfix" id="page"><!-- column -->
   <div class="position_content" id="page_position_content">
    <div class="clearfix colelem" id="pu141"><!-- group -->
     <div class="browser_width grpelem" id="u141-bw">
      <div id="u141"><!-- column -->
       <div class="clearfix" id="u141_align_to_page">
        <div class="clearfix colelem" id="u143-8"><!-- content -->
         <h1 id="u143-4">Зарабатывайте от 30% <br/>в месяц на крипторынке</h1>
         <p class="ts">Сделайте Вашу мечту реальной</p>
        </div>
        <a class="nonblock nontext rounded-corners clearfix colelem" id="u160-4" href="{referral_link()}"><!-- content --><p class="ts" id="u160-2">Регистрация</p></a>
       </div>
      </div>
     </div>
     <div class="browser_width grpelem" id="u166-bw">
      <div class="gradient" id="u166"><!-- group -->
       <div class="clearfix navbar" id="u166_align_to_page">
        <div class="clip_frame grpelem" id="u144"><!-- image -->
         <img class="block" id="u144_img" src="images/logo-at.png?crc=4267285585" alt="" width="236" height="51"/>
        </div>
        <a class="anchor_item grpelem" id="home"></a>
        <a class="nonblock nontext anm_swing clearfix grpelem animate" id="u649-4" ihref="#home"><!-- content --><p>Главная</p></a>
        <a class="nonblock nontext anim_swing clearfix grpelem animate" id="u652-4" href="#about"><!-- content --><p>О нас</p></a>
        <a class="nonblock nontext anim_swing clearfix grpelem animate" id="u658-4" href="#invest"><!-- content --><p>Инвесторам</p></a>
        <a class="nonblock nontext anim_swing clearfix grpelem animate" id="u664-4" href="#partner"><!-- content --><p>Партнерам</p></a>
        <a class="nonblock nontext clearfix grpelem" id="u667-4" href="{referral_link()}"><!-- content --><p>Регистрация</p></a>
       </div>
      </div>
     </div>
    </div>
    <a class="anchor_item colelem" id="about"></a>
    <div class="ts clearfix colelem" id="u169-4"><!-- content -->
     <p>Об Alt-Trade</p>
    </div>
    <div class="colelem" id="u639"><!-- simple frame --></div>
    <div class="clearfix colelem" id="pu276"><!-- group -->
     <div class="browser_width grpelem" id="u276-bw">
      <div class="museBGSize" id="u276"><!-- simple frame --></div>
     </div>
     <div class="ts clearfix grpelem" id="u172-6"><!-- content -->
      <p id="u172-2">Компания Alt-Trade — дружная команда профессионалов из разных стран мира. Успешные трейдеры и финансисты сплоченно трудятся над разработкой наилучшей инвестиционной платформы по работе с криптовалютами. Финансовые эксперты делают упор не только на Bitcoin, но и ставят акцент на альткоинах, как на «цифровой» валюте будущего.</p>
      <p id="u172-4">Главная цель Alt-Trade — развивать технологии Blockchain, зарабатывать на трейдинге криптовалютой и создавать на основании этого грандиозное и успешное партнерское сообщество. Мы предоставляем каждому возможность без особых знаний и усилий надежно увеличивать свой капитал с помощью передовой платформы Alt-Trade.</p>
     </div>
     <div class="clearfix grpelem" id="u600"><!-- group -->
      <div class="clearfix grpelem" id="pu244"><!-- column -->
       <div class="clip_frame colelem" id="u244"><!-- image -->
        <img class="block" id="u244_img" src="images/linea_43(3)_128.png?crc=3996552142" alt="" width="128" height="128"/>
       </div>
       <div class="clearfix colelem" id="u205-4"><!-- content -->
        <p>Выгодно</p>
       </div>
       <div class="clearfix colelem" id="u221-4"><!-- content -->
        <p>Инвестор до 100 дней возвращает тело и зарабатывает на полном автомате до конца года</p>
       </div>
      </div>
      <div class="clearfix grpelem" id="pu258"><!-- column -->
       <div class="clip_frame colelem" id="u258"><!-- image -->
        <img class="block" id="u258_img" src="images/linea_a4(1)_128.png?crc=4047071083" alt="" width="128" height="128"/>
       </div>
       <div class="clearfix colelem" id="u211-4"><!-- content -->
        <p>Удобно</p>
       </div>
       <div class="clearfix colelem" id="u227-6"><!-- content -->
        <p>Автоматизированная платформа <br/>ALT-TRADE позволить Вам управлять финансами с любой точки мира</p>
       </div>
      </div>
      <div class="clearfix grpelem" id="pu251"><!-- column -->
       <div class="clip_frame colelem" id="u251"><!-- image -->
        <img class="block" id="u251_img" src="images/linea_64(2)_128.png?crc=387595927" alt="" width="128" height="128"/>
       </div>
       <div class="clearfix colelem" id="u214-4"><!-- content -->
        <p>Прибыльно</p>
       </div>
       <div class="clearfix colelem" id="u230-4"><!-- content -->
        <p>Годовая прибыль превышает 360%, а доход начисляется каждый день с плавающим процентом</p>
       </div>
      </div>
      <div class="clearfix grpelem" id="pu236"><!-- column -->
       <div class="clip_frame colelem" id="u236"><!-- image -->
        <img class="block" id="u236_img" src="images/linea_1fa(0)_128.png?crc=4186049893" alt="" width="128" height="128"/>
       </div>
       <div class="clearfix colelem" id="u217-4"><!-- content -->
        <p>Надежно</p>
       </div>
       <div class="clearfix colelem" id="u233-4"><!-- content -->
        <p>Вы сами управляете своим инвестиционным портфелем и можете выводить деньги каждый день</p>
       </div>
      </div>
     </div>
    </div>
    <div class="clearfix colelem" id="pu567"><!-- group -->
     <div class="browser_width grpelem" id="u567-bw">
      <div class="museBGSize" id="u567"><!-- simple frame --></div>
     </div>
     <div class="browser_width grpelem" id="u578-bw">
      <div class="rgba-background" id="u578"><!-- column -->
       <div class="clearfix" id="u578_align_to_page">
        <div class="ts clearfix colelem" id="u283-4"><!-- content -->
         <p>годовой инвестиционный план</p>
        </div>
        <div class="clip_frame colelem" id="u454"><!-- image -->
         <img class="block" id="u454_img" src="images/preza1.png?crc=4016074417" alt="" width="1200" height="534"/>
        </div>
       </div>
      </div>
     </div>
     <a class="anchor_item grpelem" id="invest"></a>
    </div>
    <div class="ts clearfix colelem" id="u406-4"><!-- content -->
     <p>Как стать инвестором?</p>
    </div>
    <div class="colelem" id="u642"><!-- simple frame --></div>
    <div class="ts clearfix colelem" id="u409-8"><!-- content -->
     <p id="u409-4">Каждый совершеннолетний может стать инвестором Alt-Trade и зарабатывать <br/>от 30% в месяц от суммы открытия депозита на протяжении года</p>
     <p id="u409-6">Чтобы стать инвестором пройдите три простых шага</p>
    </div>
    <div class="clearfix colelem" id="pu412"><!-- group -->
     <div class="clip_frame grpelem" id="u412"><!-- image -->
      <img class="block" id="u412_img" src="images/linea_21(2)_128.png?crc=367817172" alt="" width="128" height="128"/>
     </div>
     <div class="clip_frame grpelem" id="u431"><!-- image -->
      <img class="block" id="u431_img" src="images/linea_1f5(0)_128.png?crc=464520796" alt="" width="128" height="128"/>
     </div>
     <div class="clip_frame grpelem" id="u447"><!-- image -->
      <img class="block" id="u447_img" src="images/linea_1de(1)_128.png?crc=382351942" alt="" width="128" height="128"/>
     </div>
    </div>
    <div class="clearfix colelem" id="pu422-4"><!-- group -->
     <div class="clearfix grpelem" id="u422-4"><!-- content -->
      <p>Зарегистрируйтесь</p>
     </div>
     <div class="clearfix grpelem" id="u419-4"><!-- content -->
      <p>Откройте депозит</p>
     </div>
     <div class="clearfix grpelem" id="u444-4"><!-- content -->
      <p>выводите прибыль</p>
     </div>
    </div>
    <div class="clearfix colelem" id="ppu428-4"><!-- group -->
     <div class="clearfix grpelem" id="pu428-4"><!-- column -->
      <div class="clearfix colelem" id="u428-4"><!-- content -->
       <p>Откройте депозит от $10 и получайте ежедневную прибыль на протяжении года</p>
      </div>
      <div class="clearfix colelem" id="pu464"><!-- group -->
       <div class="clip_frame grpelem" id="u464"><!-- image -->
        <img class="block" id="u464_img" src="images/bitcoin_logo_horizontal_light.png?crc=402129764" alt="" width="176" height="37"/>
       </div>
       <div class="clip_frame grpelem" id="u486"><!-- image -->
        <img class="block" id="u486_img" src="images/payeer.png?crc=465569275" alt="" width="176" height="40"/>
       </div>
      </div>
     </div>
     <div class="clearfix grpelem" id="pu441-4"><!-- column -->
      <div class="clearfix colelem" id="u441-4"><!-- content -->
       <p>Выводите деньги каждый день. Заявка на вывод обрабатывается до 48 часов</p>
      </div>
      <div class="clearfix colelem" id="pu472"><!-- group -->
       <div class="clip_frame grpelem" id="u472"><!-- image -->
        <img class="block" id="u472_img" src="images/etherium.png?crc=3967185935" alt="" width="176" height="44"/>
       </div>
       <div class="clip_frame grpelem" id="u493"><!-- image -->
        <img class="block" id="u493_img" src="images/payza.png?crc=3824296098" alt="" width="176" height="49"/>
       </div>
      </div>
     </div>
     <div class="clearfix grpelem" id="u425-6"><!-- content -->
      <p>Для регистрации нажмите на кнопку <br/>«Регистрация» и заполните форму</p>
     </div>
     <div class="clip_frame grpelem" id="u479"><!-- image -->
      <img class="block" id="u479_img" src="images/img-0852.png?crc=3870585581" alt="" width="176" height="49"/>
     </div>
     <div class="clip_frame grpelem" id="u500"><!-- image -->
      <img class="block" id="u500_img" src="images/perfect-money.png?crc=3962902616" alt="" width="176" height="36"/>
     </div>
    </div>
    <a class="nonblock nontext rounded-corners clearfix colelem" id="u438-4" href="{referral_link()}"><!-- content --><p class="ts" id="u438-2">Регистрация</p></a>
    <div class="browser_width colelem" id="u581-bw">
     <div class="museBGSize" id="u581"><!-- column -->
      <div class="clearfix" id="u581_align_to_page">
       <a class="anchor_item colelem" id="partner"></a>
       <div class="ts clearfix colelem" id="u524-4"><!-- content -->
        <p>ПАРТНЕРСКАЯ КОМИССИЯ</p>
       </div>
       <div class="colelem" id="u645"><!-- simple frame --></div>
       <div class="ts clearfix colelem" id="u525-8"><!-- content -->
        <p id="u525-4">Стройте карьеру вместе с Alt-Trade и зарабатывайте комиссию <br/>с дохода привлеченного инвестора целый год</p>
        <p id="u525-6">Для участия в партнерской программе пройдите регистрацию</p>
       </div>
       <div class="clearfix colelem" id="ppu530-4"><!-- group -->
        <div class="clearfix grpelem" id="pu530-4"><!-- column -->
         <div class="clearfix colelem" id="u530-4"><!-- content -->
          <p>Первая линия</p>
         </div>
         <div class="clearfix colelem" id="u557-5"><!-- content -->
          <p>10<span class="superscript">%</span></p>
         </div>
        </div>
        <div class="clearfix grpelem" id="pu533-4"><!-- column -->
         <div class="clearfix colelem" id="u533-4"><!-- content -->
          <p>Вторая линия</p>
         </div>
         <div class="clearfix colelem" id="u554-5"><!-- content -->
          <p>4<span class="superscript">%</span></p>
         </div>
        </div>
        <div class="clearfix grpelem" id="pu536-4"><!-- column -->
         <div class="clearfix colelem" id="u536-4"><!-- content -->
          <p>Третья линия</p>
         </div>
         <div class="clearfix colelem" id="u551-5"><!-- content -->
          <p>1.5<span class="superscript">%</span></p>
         </div>
        </div>
        <div class="clearfix grpelem" id="pu539-4"><!-- column -->
         <div class="clearfix colelem" id="u539-4"><!-- content -->
          <p>Четвертая линия</p>
         </div>
         <div class="clearfix colelem" id="u548-5"><!-- content -->
          <p>1<span class="superscript">%</span></p>
         </div>
        </div>
        <div class="clearfix grpelem" id="pu542-4"><!-- column -->
         <div class="clearfix colelem" id="u542-4"><!-- content -->
          <p>Пятая линия</p>
         </div>
         <div class="clearfix colelem" id="u545-5"><!-- content -->
          <p>0.5<span class="superscript">%</span></p>
         </div>
        </div>
        <div class="clearfix grpelem" id="pu560-4"><!-- column -->
         <div class="clearfix colelem" id="u560-4"><!-- content -->
          <p>Начните зарабатывать после быстрой регистрации</p>
         </div>
         <a class="nonblock nontext rounded-corners clearfix colelem" id="u564-4" href="{referral_link()}"><!-- content --><p id="u564-2">Стать партнером</p></a>
        </div>
       </div>
      </div>
     </div>
    </div>
    <div class="clearfix colelem" id="pu1034"><!-- group -->
     <div class="browser_width grpelem" id="u1034-bw">
      <div id="u1034"><!-- simple frame --></div>
     </div>
     <div class="browser_width grpelem" id="u1064-bw">
      <div id="u1064"><!-- simple frame --></div>
     </div>
     <div class="clip_frame grpelem" id="u1045"><!-- image -->
      <img class="block" id="u1045_img" src="images/ranges.png?crc=527823189" alt="" width="995" height="667"/>
     </div>
     <div class="ts clearfix grpelem" id="u1055-4"><!-- content -->
      <p>Премии за оборот</p>
     </div>
     <div class="clearfix grpelem" id="u1061-16"><!-- content -->
      <p id="u1061-2">В ОБОРОТ СЧИТАЕТСЯ:</p>
      <p id="u1061-5"><br/>° 100% с первой линии;</p>
      <p id="u1061-7">° 70% со второй линии;</p>
      <p id="u1061-9">° 50% с третьей линии;</p>
      <p id="u1061-11">° 25% с четвертой линии;</p>
      <p id="u1061-13">° 10% с пятой линии.</p>
      <p id="u1061-14">&nbsp;</p>
     </div>
    </div>
    <div class="ts clearfix colelem" id="u716-4"><!-- content -->
     <p>Отзывы об Alt-Trade</p>
    </div>
    <div class="ts clearfix colelem" id="u719-4"><!-- content -->
     <p id="u719-2">Что говорят об Alt-Trade инвесторы, которые зарабатывают с нами на криптовалюте</p>
    </div>
    <div class="PamphletWidget clearfix colelem" id="pamphletu858"><!-- none box -->
     <div class="ThumbGroup clearfix grpelem" id="u859"><!-- none box -->
      <div class="popup_anchor" id="u861popup">
       <div class="Thumb popup_element" id="u861"><!-- simple frame --></div>
      </div>
      <div class="popup_anchor" id="u913popup">
       <div class="Thumb popup_element" id="u913"><!-- simple frame --></div>
      </div>
     </div>
     <div class="popup_anchor" id="u863popup">
      <div class="ContainerGroup clearfix" id="u863"><!-- stack box -->
       <div class="Container clearfix grpelem" id="u864"><!-- group -->
        <div class="clearfix grpelem" id="pu904-8"><!-- group -->
         <div class="rounded-corners clearfix grpelem" id="u904-8"><!-- content -->
          <p id="u904-2">Здравствуйте! Меня пригласил Стас блогер, я начал изучать и мне больше всего понравилось отчёты трейдеров и хорошая доходность, благодарен своим партнёрам, что посоветовали проект, где можно и нужно работать.</p>
          <p id="u904-5">Сергей Зайцев, <span id="u904-4">Инвестор</span></p>
          <p id="u904-6">&nbsp;</p>
         </div>
         <div class="museBGSize rounded-corners grpelem" id="u942"><!-- simple frame --></div>
        </div>
        <div class="clearfix grpelem" id="pu907-8"><!-- group -->
         <div class="rounded-corners clearfix grpelem" id="u907-8"><!-- content -->
          <p id="u907-2">Отличный проект! Выплаты быстро и регулярно приходят и доходность хорошая! Мой вклад 777$ думаю еще сделать депозит в биткоинах, всем желаю профита и проекту долгосрочной работы.</p>
          <p id="u907-5">Владимир, <span id="u907-4">Инвестор и блогер, Команда Kwingroup</span></p>
          <p id="u907-6">&nbsp;</p>
         </div>
         <div class="museBGSize rounded-corners grpelem" id="u953"><!-- simple frame --></div>
        </div>
        <div class="clearfix grpelem" id="pu1004-8"><!-- group -->
         <div class="rounded-corners clearfix grpelem" id="u1004-8"><!-- content -->
          <p id="u1004-2">Отличный проект, зашел на 400$, после того как узнал, что есть начислении в битках (биткоин не конвертируется), добавил еще 0,25BTC</p>
          <p id="u1004-5">Piggybankholiday, <span id="u1004-4">Инвестор</span></p>
          <p id="u1004-6">&nbsp;</p>
         </div>
         <div class="museBGSize rounded-corners grpelem" id="u1005"><!-- simple frame --></div>
        </div>
       </div>
       <div class="Container invi clearfix grpelem" id="u916"><!-- group -->
        <div class="clearfix grpelem" id="pu998-8"><!-- group -->
         <div class="rounded-corners clearfix grpelem" id="u998-8"><!-- content -->
          <p id="u998-2">Интересный проект с доходностью от 30% в месяц, а также с отчетами трейдеров. Рекомендую присмотреться :)</p>
          <p id="u998-5">Денис HyipHunter, <span id="u998-4">Инвестор и блогер</span></p>
          <p id="u998-6">&nbsp;</p>
         </div>
         <div class="museBGSize rounded-corners grpelem" id="u999"><!-- simple frame --></div>
        </div>
        <div class="clearfix grpelem" id="pu926-8"><!-- group -->
         <div class="rounded-corners clearfix grpelem" id="u926-8"><!-- content -->
          <p id="u926-2">ОТЛИЧНЫЙ И ПЕРСПЕКТИВНЫЙ ПРОЕКТ! Мой стартовый вклад 500$ Но буду добавлять!</p>
          <p id="u926-5">Кирилл Richyinvestor, <span id="u926-4">Инвестор</span></p>
          <p id="u926-6">&nbsp;</p>
         </div>
         <div class="museBGSize rounded-corners grpelem" id="u962"><!-- simple frame --></div>
        </div>
        <div class="clearfix grpelem" id="pu927-8"><!-- group -->
         <div class="rounded-corners clearfix grpelem" id="u927-8"><!-- content -->
          <p id="u927-2">Годный проект, радующий честными выплатами. Спасибо админу!</p>
          <p id="u927-5">Михаил Елисеев, <span id="u927-4">Инвестор, Команда «Компас Инвестиций»</span></p>
          <p id="u927-6">&nbsp;</p>
         </div>
         <div class="museBGSize rounded-corners grpelem" id="u965"><!-- simple frame --></div>
        </div>
       </div>
      </div>
     </div>
     <div class="popup_anchor" id="u867popup">
      <div class="PamphletPrevButton PamphletLightboxPart popup_element clearfix" id="u867"><!-- group -->
       <div class="rounded-corners clearfix grpelem" id="u868-4"><!-- content -->
        <p>&lt;</p>
       </div>
      </div>
     </div>
     <div class="popup_anchor" id="u869popup">
      <div class="PamphletNextButton PamphletLightboxPart popup_element clearfix" id="u869"><!-- group -->
       <div class="rounded-corners clearfix grpelem" id="u870-4"><!-- content -->
        <p>&gt;</p>
       </div>
      </div>
     </div>
    </div>
    <div class="clearfix colelem" id="pu603"><!-- group -->
     <div class="browser_width grpelem" id="u603-bw">
      <div class="museBGSize" id="u603"><!-- simple frame --></div>
     </div>
     <div class="browser_width grpelem" id="u617-bw">
      <div class="rgba-background" id="u617"><!-- column -->
       <div class="clearfix" id="u617_align_to_page">
        <div class="ts clearfix colelem" id="u614-6"><!-- content -->
         <p id="u614-2">Начните зарабатывать на криптовалюте</p>
         <p id="u614-4">Зарегистрируйтесь на платформе Alt-Trade прямо сейчас</p>
        </div>
        <a class="nonblock nontext clearfix colelem" id="u620-4" href="{referral_link()}"><!-- content --><p class="ts" id="u620-2">Регистрация</p></a>
       </div>
      </div>
     </div>
    </div>
    <div class="browser_width colelem" id="u623-bw">
     <div id="u623"><!-- group -->
      <div class="clearfix" id="u623_align_to_page">
       <div class="clearfix grpelem" id="u636-4"><!-- content -->
        <p>© 2017 Alt-Trade. Все права защищены</p>
       </div>
      </div>
     </div>
    </div>
    <div class="verticalspacer" data-offset-top="5905" data-content-above-spacer="5905" data-content-below-spacer="0"></div>
   </div>
  </div>
  
  <script type="text/javascript" src="/js/landing.js"></script>

   </body>
</html>