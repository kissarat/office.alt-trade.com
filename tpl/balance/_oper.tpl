{strip}

	
	<form method="post" name="{$edit_form_name}">
	<div class="table-responsive">
                <table class="table">
                  <tbody>
		<input name="oID" id="{$edit_form_name}_oID" value="{$el.oID}" type="hidden">
		<tr>
			<th>{$_TRANS['Status']}</th>
			<td>{$op_statuses[$el.oState]}</td>
		</tr>
		<tr>
			<th>{$_TRANS['Date']}</th>
			<td>{$el.oCTS}</td>
		</tr>
        {if $from_admin}
        	<tr>
        		<th>{$_TRANS['User']}</th>
        		<td><a href="{_link module='account/admin/user'}?id={$el.uID}">{$el.uLogin}</a></td>
        	</tr>
        {/if}
        {if $el.oParams.uid2}
        	<th>{valueIf($el.oOper == 'TR', $_TRANS['Receiver'], $_TRANS['Sender'])}</th>
        	<td>
        		{valueIf($from_admin, "<a href=\"{_link module='account/admin/user'}?id={$el.oParams.uid2}\">{$el.oParams.user}</a>", $el.oParams.user)}
        	</td>
        {/if}
        <tr>
        	<th>{$_TRANS['Curr']}</th>
        	<td>{$el.ocCurrID}</td>
        </tr>
        <tr>
        	<th>{$_TRANS['Amount']}</th>
        	<td>{_z($el.oSum, $el.ocCurrID, 1)}</td>
        </tr>
        {if ($el.oComis != 0)}
	        <tr>
				<th>{$_TRANS['Comission']}</th>
				<td>{_z($el.oComis, $el.ocCurrID, 1)}</td>
			</tr>
        {/if}
        {if ($el.oComis != 0)}
        	<tr>
        		<th>{$op_sums[$el.oOper]}</th>
        		<td>{_z($el.oSum - $el.oComis, $el.ocCurrID, 1)}</td>
        	</tr>
	        <div class="block_form_el text_type cfix">
	            <label></label>
	            <div class="block_form_el_right"><i></i></div>
	        </div>
        {/if}
		{if in_array($el.oOper, ['BONUS', 'PENALTY', 'CASHIN', 'CASHOUT', 'TR', 'TRIN'])}
			<tr>
				<th>{$_TRANS['Payment system']}</th>
				<td>{valueIf($from_admin, "<a href=\"{_link module='balance/admin/curr'}?id={$el.ocID}\">{$el.cName}</a>", $el.cName)}</td>
			</tr>
		{/if}
        {if ($el.oSum2 > 0)}
        	<tr>
        		<th>{$_TRANS['Amount to receive']}</th>
        		<td>{_z($el.oSum2, exValue($el.ocID, $el.ocCurrID2), 1)}</td>
        	</tr>
        {/if}
        {if $smarty.get.payto}
        	<tr>
        		<th>{$_TRANS['Pay to account']}</th>
        		<td>{$smarty.get.payto}</td>
        	</tr>
	    {/if}
        {if ($el.oBatch)}
        	<tr>
        		<th>{$_TRANS['Batch-number']}</th>
        		<td>{$el.oBatch}</td>
        	</tr>
        {/if}
        {if ($el.oMemo)}
        	<tr>
        		<th>{$_TRANS['Memo']}</th>
        		<td>{valueIf(!$from_admin and ($el.oMemo[0] == '~'), {$_TRANS['Error']}, $el.oMemo)}</td>
        	</tr>
        {/if}
        {if ($el.oTS)}
        	<tr>
        		<th>{$_TRANS['Modified']}</th>
        		<td>{$el.oTS}</td>
        	</tr>
        {/if}
        {if ($el.oNTS)}
        	<tr>
        		<th>{$_TRANS['Completed']}</th>
        		<td>{$el.oNTS}</td>
        	</tr>
        {/if}

        {if $pvalues.acc}
        	<tr>
        		<th>{$_TRANS['Receiver']}</th>
        		<td>{$pvalues['acc']}</td>
        	</tr>
        	<div class="block_form_el text_type cfix">
	            <label></label>
	            <div class="block_form_el_right"><i>{$el.oNTS}</i></div>
	        </div>
        	
        {/if}

        {if $el.oParams2.acc}
            <tr>
                <th>{valueIf($el.oOper == 'CASHOUT', $_TRANS['Payee account'], $_TRANS['Payeer account'])}</th>
                <td>{$el.oParams2.acc}</td>
            </tr>
        {/if}

        {_getFormSecurity form=$edit_form_name}


        </tbody>
</table>
</div>
		<br>
        {if $bt !== ' '}
        	<br>
			<input name="{$edit_form_name}_btn" value="{if $bt}{$bt}{elseif !$edit_is_new}Сохранить{else}Создать{/if}" type="submit" class="btn btn-primary">
		{/if}

        {if is_array($b)}
			{foreach from=$b key=f item=v}
				&nbsp;<input name="{$edit_form_name}_btn{$f}" value="{$v}" onClick="return confirm('Подтвердите операцию \'{$v}\'');" type="submit" class="btn btn-primary">
			{/foreach}
		{/if}
	</form>

{if $afields}

	<br>
	<p class="info">
		{$_TRANS['For current payment system not set']} <a href="{_link module='balance/admin/curr'}?id={$el.ocID}">{$_TRANS['API settings']}</a><br>
		{$_TRANS['You can perform this operation by specifying the settings above']}. <br>
		{$_TRANS['It is safe, because entered data are not stored anywhere']}
	</p>

{/if}

{if $el.oOper == 'CASHIN'}

	{if !isset($smarty.get.payto)}
		{include file='balance/_pform.tpl' btn_text=$_TRANS['Pay through the merchant']}
	{/if}

    {if $pvalues.acc or ($from_admin and ($el.oState <= 2))}

        {include file='balance/_oper.data.tpl' oComis=0}

    {/if}

{elseif ($el.oOper == 'CASHOUT') and ($from_admin and ($el.oState <= 2))}

	{include file='balance/_oper.data.tpl' oComis=$el.oComis}

{/if}

{/strip}