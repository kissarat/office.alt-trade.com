{strip}
{include file='header.tpl' title=$_TRANS['Balance'] class="cabinet"}

<div class="row balance">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>{$_TRANS['operations']}</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                
                {if isset($smarty.get.fail)}

                    <h1>{$_TRANS['Operation NOT complete!']}</h1>
                    <p class="info">
                        {$_TRANS['The operation was was aborted or an error occurs']}.<br>
                        {$_TRANS['Try']} <a href="{$_selfLink}">{$_TRANS['again']}</a> {$_TRANS['later']}
                    </p>

                {else}

                {include file='balance/_statuses.tpl'}

                <script type="text/javascript" src="static/admin/js/lists.js"></script>

                {include file='edit.begin.tpl' form='opers_filter' url='*'}

                <link rel="stylesheet" type="text/css" media="all" href="{$home_site}static/admin/js/jscalendar/calendar-blue.css" title="win2k-cold-1" />
                <script type="text/javascript" src="{$home_site}static/admin/js/jscalendar/calendar.js"></script>
                <script type="text/javascript" src="{$home_site}static/admin/js/jscalendar/calendar-ru.js"></script>
                <script type="text/javascript" src="{$home_site}static/admin/js/jscalendar/calendar-setup.js"></script>

                <table class="table table-striped jambo_table">
                    <thead>
                        <tr>
                            <th>{$_TRANS['Date']}</th>
                            <th>{$_TRANS['Operation']}</th>
                            <th>Реферал</th>
                            <th>{$_TRANS['Payment system']}</th>
                            <th>{$_TRANS['Out']}</th>
                            <th>{$_TRANS['Comming to account']}</th>
                            <th>{$_TRANS['State']}</th>
                        </tr>
                    </thead>

                    <tbody>
                        {foreach from=$list item=l}
                            <tr>
                                <td>{$l.oCTS}</td>
                                <td>
                                    {if $l.oNTS}
                                        {$op_names[$l.oOper]}
                                    {else}
                                        {$op_names[$l.oOper]}
                                    {/if}
                                </td>
                                <td>
                                    {if $l.oOper == 'REF'}
                                        {$l.oParams['reflogin']}
                                    {/if}
                                </td>
                                <td>
                                    {if in_array($l.oOper, ['BONUS', 'PENALTY', 'CASHIN', 'CASHOUT', 'TR', 'TRIN'])}
                                        <small>{$l.cName}</small><br>
                                                    {_z($l.oSum2, $l.ocID, 1)}
                                    {/if}
                                </td>
                                <td>
                                    {if in_array($l.oOper, array('PENALTY', 'CASHOUT', 'EX', 'TR', 'BUY', 'GIVE', 'CALCOUT'))}
                                        <span style="color: red;">{_z($l.oSum, $l.ocCurrID, 1)}</span>
                                        {if $l.oComis != 0}
                                            <br>
                                            <sup>{_z($l.oComis, $l.ocCurrID, 1)}</sup>
                                        {/if}
                                    {/if}
                                </td>
                                <td>
                                    {if in_array($l.oOper, array('EX'))}
                                        {$l.oSum = $l.oSum2}
                                        {$l.ocCurrID = $l.ocCurrID2}
                                    {/if}
                                    {if in_array($l.oOper, array('BONUS', 'CASHIN', 'EX', 'TRIN', 'SELL', 'REF', 'TAKE', 'CALCIN'))}
                                        {_z($l.oSum, $l.ocCurrID, 1)}
                                        {if $l.oComis != 0}
                                            <br>
                                            <sup>{_z($l.oComis, $l.ocCurrID, 1)}</sup>
                                        {/if}
                                    {/if}
                                </td>
                                <td>
                                    {if $l.oNTS}
                                        {$op_statuses[$l.oState]}
                                    {else}
                                        {$op_statuses[$l.oState]}
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}        
                    </tbody>
                </table>
                {include file='pl.tpl' linkparams=$linkparams}

                {/if}

            </div>
        </div>
    </div>
</div>

{include file='footer.tpl'  class="cabinet"}
{/strip}