{strip}
{include file='header.tpl'}


<div class="x_panel">
	<div class="x_title">
		<h2>{$_TRANS['Payment details']}</h2>
		<div class="clearfix"></div>
	</div>

	<div class="x_content">
		<ul class="nav nav-tabs bar_tabs">
			<li role="presentation"><a href="/profile">{$_TRANS['Profile']}</a></li>
		  <li role="presentation"><a href="/account">{$_TRANS['Security']}</a></li>
		  <li role="presentation"><a href="{_link module='account/change_pass'}">{$_TRANS['Change password']}</a></li>
		  <li role="presentation" class="active"><a href="/balance/wallets">{$_TRANS['Payment details']}</a></li>
		</ul>
	

{if !$wfields}

	<p class="info">
		{$_TRANS['Payment systems are not connected']}
	</p>

{else}

	<div class="col-md-6 col-xs-12">
        {$_TRANS['bal_save']}


		{if $tpl_errors['balance/wallets_frm']|count}
			<ul class="error_message">
		    	{$_TRANS['Please fix the following errors']}:<br/>
		        {if $tpl_errors['balance/wallets_frm'][0]=='psys_wrong'}<li>{$_TRANS['Input pay.system']}</li>{/if}
		        {if $tpl_errors['balance/wallets_frm'][0]=='pin_wrong'}<li>{$_TRANS['Wrong code']}</li>{/if}
		    </ul>
		{/if}

		<form method="post" action="/balance/wallets" name="balance/wallets_frm">
			<div class="form-group">
				<label for="">{$_TRANS['Default payment system']}</label>
				<div class="block_form_el_right">
					<select name="DefCurr" id="balance/wallets_frm_DefCurr" class="form-control">
						<option value="0" selected="">- {$_TRANS['select']} -</option>
						{foreach from=$defcurr key=k item=v}
							<option value="{$k}" {if $user['aDefCurr']==$k}selected="selected"{/if}>{$v}</option>
						{/foreach}
					</select>
				</div>
			</div>
			{foreach from=$wfields key=f item=v}
			<div class="form-group">
				<label for="">{$v[1]}</label>
				<div class="block_form_el_right">
					{$vv = _arr_val($wdata, $f)}
					<input name="{$f}" id="balance/wallets_frm_{$f}" value="{$vv}" type="text" class="form-control">
					<small>{$v.comment}</small>
				</div>
			</div>
			{/foreach}
			{if $showbutton and ($_cfg.Sec_MinPIN > 0)}
				<div class="form-group">
					<label for="">{$_TRANS['Input PIN-code (to confirm changes)']}</label>
					<div class="block_form_el_right">
						<input name="PIN" id="balance/wallets_frm_PIN" value="{$smarty.request.PIN}" type="text" class="form-control">
					</div>
				</div>
			{/if}

			{_getFormSecurity form='balance/wallets_frm'}
			<input name="balance/wallets_frm_btn" value="{$_TRANS['Save']}" type="submit" class="btn btn-info">
		</form>
	</div>

{/if}

</div>

{include file='footer.tpl' class="cabinet"}
{/strip}