<div class="row balance">
    <div class="col-md-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>{$_TRANS['refill_balance']}</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

      {if $tpl_errors|count}
              <ul class="error_message">
                {$_TRANS['Please fix the following errors']}:<br/>
                  {if $tpl_errors['add'][0]=='psys_empty'}<li>{$_TRANS['Select pay.system']}</li>{/if}
                  {if $tpl_errors['add'][0]=='sum_wrong'}<li>{$_TRANS['Wrong amount']}</li>{/if}
                  {if $tpl_errors['add'][0]=='limit_exceeded'}<li>{$_TRANS['Limit exceeded']}</li>{/if}
                  {if $tpl_errors['add'][0]=='pin_wrong'}<li>{$_TRANS['Wrong code']}</li>{/if}
                  {if $tpl_errors['add'][0]=='plan_wrong'}<li>{$_TRANS['Wrong plan']}</li>{/if}
                  {if $tpl_errors['add'][0]=='compnd_wrong'}<li>{$_TRANS['Wrong value']} ({$cmin}..{$cmax})</li>{/if}
                  {if $tpl_errors['add'][0]=='compnd_wrong1'}<li>{$_TRANS['Wrong value for plan']} '{$cplan}'</li>{/if}
                  {if $tpl_errors['add'][0]=='oper_disabled'}<li>{$_TRANS['Operation disabled']}</li>{/if}
              </ul>
          {/if}

          <form method="post" name="add">
            <input name="Oper" id="add_Oper" value="CASHIN" type="hidden">
            

        <div class="form-group">
          <label for="">{$_TRANS['Currency account']}<span class="descr_star">*</span></label>
          <div class="block_form_el_right">
            <select class="form-control" name="Curr" id="add_Curr">
              <option selected="" value="0">- {$_TRANS['select']} -</option>
              <option value="USD">USD</option>
              <option value="BTC">BTC</option>
              {* {foreach $currs as $cid => $c}
                  <option value="{$cid}" {if $cid eq $smarty.post.Curr}selected{/if}>{$cid}</option>
              {/foreach} *}
            </select>
          </div>
        </div>

        <div class="form-group">
          <label for="">{$_TRANS['Amount']}<span class="descr_star">*</span></label>
          <div class="block_form_el_right">
            <input class="form-control" name="Sum" id="add_Sum" value="{$smarty.request.Sum}" type="text">
          </div>
        </div>
		
        <div class="form-group">
          <label for="">{$_TRANS['From payment system']}<span class="descr_star">*</span></label>
          <div class="block_form_el_right">
            <select name="PSys" id="add_PSys" class="form-control">
              <option selected="" value="0">- {$_TRANS['select']} -</option>
              {foreach from=$clist item=i key=k}
                <option value="{$k}" id="{$i['curr']}" {if ($user.aDefCurr)==$k}selected="selected"{/if}>{$i['name']}</option>
              {/foreach}
            </select>
          </div>
        </div>

        {if $plans}
          <div class="form-group">
            <label for="">{$_TRANS['Plan']}</label>
            <div class="block_form_el_right">
              <select name="Plan" id="add_Plan" class="form-control">
                <option value="0">- {$_TRANS['auto']} -</option>
                {foreach from=$plans item=i key=k}
                  <option value="{$k}">{$i}</option>
                      {/foreach}
              </select>
            </div>
          </div>
        {/if}
		
        {if $pcmax}
          <div class="form-group">
            <label for="">{$_TRANS['Compaund']}</label>
            <div class="block_form_el_right">
              <input name="Compnd" id="add_Compnd" value="" type="text" class="form-control">
            </div>
          </div>
        {/if}
        <br>
        <div class="form-group">
          {_getFormSecurity form='add'}
            <input name="add_btn" value="{$_TRANS['Create']}" type="submit" class="btn btn-primary">
        </div>
        
      </form>
    </div>
        </div>
    </div>
    <div class="col-md-6">
      <div class="x_panel">
        <div class="x_title">
          <h2>{$_TRANS['Information']}</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <ul class="list-group">
            <li class="list-group-item">{$_TRANS['Minimal Cash']}</li>
          </ul>
          <h4>{$_TRANS['Info refill']}</h4>
          <h4>* {$_TRANS['bitcoin_info_add']}</h4>
          <h4>* {$_TRANS['bitcoin_info_after']}</h4>
        </div>
      </div>
    </div>
</div>

<script>
	$('#add_Curr').change(function () {
		var curr = '#' + $(this).val();

		var el = $('#add_PSys');
		var options = el.find('option').hide();

		var op = el.find('option' + curr).show();
		el.val(op.first().val());
	});
</script>