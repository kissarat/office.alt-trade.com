<div class="row balance">
    <div class="col-md-6 col-xs-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>{$_TRANS['withdrawal']}</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
        {$s = valueIf($_cfg.Const_IntCurr, $_TRANS['Amount (in internal units)'], $_TRANS['Amount'])}

        <div class="block_form">
        {if $tpl_errors|count}
{*print_r($tpl_errors, 1)*}
            {*xEcho($tpl_errors['add'][0])*}
                <ul class="error_message">
                  {$_TRANS['Please fix the following errors']}:<br/>
                    {if $tpl_errors['add'][0]=='wallet_not_defined'}<li>{$_TRANS['Put your wallet']}</li>{/if}
                    {if $tpl_errors['add'][0]=='psys_empty'}<li>{$_TRANS['Pay.system wrong']}</li>{/if}
                    {if $tpl_errors['add'][0]=='sum_wrong'}<li>{$_TRANS['Calculator']}</li>{/if}
                    {if $tpl_errors['add'][0]=='limit_exceeded'}<li>{$_TRANS['Limit exceeded']}</li>{/if}
                    {if $tpl_errors['add'][0]=='pin_wrong'}<li>{$_TRANS['Wrong code']}</li>{/if}
                    {if $tpl_errors['add'][0]=='low_bal1'}<li>{$_TRANS['Insufficient funds']}</li>{/if}
                    {if $tpl_errors['add'][0]=='oper_disabled'}<li>{$_TRANS['Operation disabled']}</li>{/if}   
                    {if $tpl_errors['add'][0]=='sum_max'}<li>{$_TRANS['sum_max']}</li>{/if}
                    {if $tpl_errors['add'][0]=='sum_min'}<li>{$_TRANS['sum_min']}</li>{/if}      
                    {if $tpl_errors['add'][0]=='wallet_not_defined'}<li>{$_TRANS['wallet_not_defined']}</li>{/if}      
          </ul>
        {/if}

          <form method="post" name="add">
            <input name="Oper" id="add_Oper" value="CASHOUT" type="hidden">
          <div class="form-group">
          <label>{$_TRANS['Currency account']}<span class="descr_star">*</span></label>
          <div>
            <select class="form-control" name="Curr" id="add_Curr">
              <option selected="" value="0">- {$_TRANS['select']} -</option>
              {foreach $currs as $cid => $c}
        {if $c.Bal > 0}
                  <option value="{$cid}" id="currency" {if $cid eq $smarty.post.Curr}selected{/if}>{$cid}</option>
        {/if}
              {/foreach}
            </select>
          </div>
        </div>
        
        <div class="form-group">
          <label for="">{$_TRANS['Amount']}<span class="descr_star">*</span></label>
          <div class="block_form_el_right">
            <input class="form-control" name="Sum" id="add_Sum" value="{$smarty.request.Sum}" type="text">
          </div>
        </div>
    
        <div class="form-group">
          <label for="">{$_TRANS['On payment system']}<span class="descr_star">*</span></label>
          <div class="block_form_el_right">
            <select name="PSys" id="add_PSys" class="form-control">
              <option value="0">- {$_TRANS['select']} -</option>
              {foreach from=$clist item=i key=k}
                <option value="{$k}" id="{$i['curr']}" {if ($user.aDefCurr)==$k}selected="selected"{/if}>{$i['name']}</option>
              {/foreach}
            </select>
          </div>
        </div>


        <div class="clearfix"></div>

        

        {_getFormSecurity form='add'}
            {if $cashin && empty($cashout)}
            	<br>
              <input name="add_btn" value="{$_TRANS['Create']}" type="submit" class="btn btn-primary">
            {/if}
            
      </form>
    </div>

      </div>  
    </div>
</div>
<div class="col-md-6">
      <div class="x_panel">
        <div class="x_title">
          <h2>{$_TRANS['Information']}</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <ul class="list-group">
            <li class="list-group-item">{$_TRANS['Minimal cashout']}</li>
          </ul>
          <h4>* {$_TRANS['Info settings']}</h4>
          <h4>* {$_TRANS['Processing of the withdrawal']}</h4>
        </div>
      </div>
    </div>
<script>

$('#add_Curr').change(function () {
	var curr = '#' + $(this).val();

	var el = $('#add_PSys');
	var options = el.find('option').hide();

	var op = el.find('option' + curr).show();
	el.val(op.first().val());
});

</script>

</div>