{include file='header.tpl'}

<div class="col-md-12 col-sm-12 col-xs-12">
<div class="x_panel">
  <div class="x_title" style="border-bottom: 0">
    <h2>{$_TRANS['Stats']}</h2>
    <ul class="nav navbar-right panel_toolbox">
    <div class="btn-group btn-stats" role="group">
      <button type="button" class="btn btn-default" value="perc">{$_TRANS['Percentages']}</button>
      <button type="button" class="btn btn-default" value="month">{$_TRANS['Month']}</button>
      <button type="button" class="btn btn-primary" value="day">{$_TRANS['Day']}</button>
    </div>
    </ul>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <div id="echarts" style="height: 400px;"></div>
  </div>
</div>
</div>

<div class="depo">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>{$_TRANS['Deposits']}</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
        {if $list}
          <table class="table table-striped jambo_table">
            <thead>
              <tr>
                <th>{$_TRANS['Created']}</th>
                <th>{$_TRANS['Amount']}</th>
                <th>{$_TRANS['Plan']}</th>
                <th>{$_TRANS['Accruals count']}</th>
                <th>{$_TRANS['<small>Profit</small>']|html_entity_decode}</th>
                <th>{$_TRANS['Next accrual']}</th>
              </tr>
            </thead>
            <tbody>
              {foreach from=$list key=key item=i}
                {if $i['dState'] == '1'}
                  <tr>
                    <td>{date('d.m.Y', $i['dCTS']|stampToTime)}</td>
                    <td>
                      {if $i['dcCurrID'] == 'USD'}
                        ${$i['dZD']|string_format:"%d"}
                      {else}
                        &#3647;{$i['dZD']|string_format:"%.4f"}
                      {/if}
                    </td>
                    <td>{$i['pName']}</td>
                    <td>
                      {$i['pDays'] - ((($i['dETS']|stampToTime - time())/60/60/24)|string_format:"%d") - 1}
                      &nbsp;/&nbsp; 
                      {$i['pDays']}
                    </td>
                    <td>
                      {if $i['dcCurrID'] == 'USD'}
                        ${$i['dZP']|string_format:"%.2f"}
                      {else}
                        &#3647;{$i['dZP']|string_format:"%.4f"}
                      {/if}
                    </td>
                    <td>{$i['dNTS']}</td>
                  </tr>
                {/if}
              {/foreach}
            </tbody>
          </table>
          
        {else}

          {$_TRANS['You <b>do not have deposits']|html_entity_decode}</b>
          <br>
          <br>

        {/if}

        <a href="{_link module='depo/depo'}?add" class="btn btn-primary">{$_TRANS['Make deposit']}</a>

            </div>
        </div>
    </div>
</div>

<script src="/js/echarts.min.js"></script>
<script src="/js/echart-stats.js"></script>

{include file='footer.tpl'}