<div class="row tile_count">
	<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
		<span class="count_top"><i class="fa fa-balance-scale"></i> {$_TRANS['Balance']}</span>
		<div class="count green">
			<div class="amount">
				{if $currs['USD']['Bal'] > 0}
					${$currs['USD']['Bal']|string_format:"%.2f"}
				{else}
					$0
				{/if}
				{if $currs['BTC']['Bal'] > 0}
					<br>
					<span>&#3647;</span>{$currs['BTC']['Bal']|floatval}
				{else}
					<br>
					<span>&#3647;</span>0
				{/if}
			</div>
		</div>
	</div>
	<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
		<span class="count_top"><i class="fa fa-clock-o"></i> {$_TRANS['contribution']}</span>
		<div class="count">
			<div class="amount">
				{if $stats['contribution']['USD'] > 0}
					${$stats['contribution']['USD']|string_format:"%d"}
				{else}
					$0
				{/if}
				{if $stats['contribution']['BTC'] > 0}
					<br>
					<span>&#3647;</span>{$stats['contribution']['BTC']|string_format:"%.3f"}
				{else}
					<br>
					<span>&#3647;</span>0
				{/if}
			</div>
		</div>
	</div>
	<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
		<span class="count_top"><i class="fa fa-money"></i> {$_TRANS['deposit_income']}</span>
		<div class="count">
			<div class="amount">
				{if $stats['depo']['USD'] > 0}
					${$stats['depo']['USD']|string_format:"%d"}
				{else}
					$0
				{/if}
				{if $stats['depo']['BTC'] > 0}
					<br>
					<span>&#3647;</span>{$stats['depo']['BTC']|string_format:"%.3f"}
				{else}
					<br>
					<span>&#3647;</span>0
				{/if}
			</div>
		</div>
	</div>
	<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
		<span class="count_top"><i class="fa fa-user"></i> {$_TRANS['partners']}</span>
		<div class="count">
			<div class="amount">
				{if $stats['ref']['USD'] > 0}
					${$stats['ref']['USD']|string_format:"%d"}
				{else}
					$0
				{/if}
				{if $stats['ref']['BTC'] > 0}
					<br>
					<span>&#3647;</span>{$stats['ref']['BTC']|string_format:"%.3f"}
				{else}
					<br>
					<span>&#3647;</span>0
				{/if}
			</div>
		</div>
	</div>
	<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
		<span class="count_top"><i class="fa fa-user"></i> {$_TRANS['Withdrawal']}</span>
		<div class="count">
			<div class="amount">
				{if $stats['cashout']['USD'] > 0}
					${$stats['cashout']['USD']|string_format:"%d"}
				{else}
					$0
				{/if}
				{if $stats['cashout']['BTC'] > 0}
					<br>
					<span>&#3647;</span>{$stats['cashout']['BTC']|string_format:"%.3f"}
				{else}
					<br>
					<span>&#3647;</span>0
				{/if}
			</div>
		</div>
	</div>
	<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
		<span class="count_top"><i class="fa fa-user"></i> {$_TRANS['Circulation']}</span>
		<div class="count">
			<div class="amount">
				{if $stats['depo_sum']['USD'] > 0}
					{if $stats['depo_sum']['BTC'] > 0}
						{$turnover = $stats['depo_sum']['USD'] + $stats['depo_sum']['BTC'] * 2500}
						${$turnover|string_format:"%d"}
					{else}
						${$stats['depo_sum']['USD']|string_format:"%d"}
					{/if}
					<br><br>
				{else}
					$0
					<br><br>
				{/if}
			</div>
		</div>
	</div>
</div>