{strip}
<!-- Trigger the modal with a button -->
<a href="" class="sign-up" data-toggle="modal" data-target="#register-modal">Регистрация</a>

<!-- Modal -->
<div id="register-modal" class="modal fade" role="dialog">
  <div class="modal-dialog register-modal">

    {include file='account/register/modal.tpl'}

  </div>
</div>
{/strip}