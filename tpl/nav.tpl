<div class="top_nav">
  <div class="nav_menu">
    <nav>
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>

      <ul class="nav navbar-nav navbar-right">

        <li>
          <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <img src="/upload/avatar/{$user_profile['aAvatar']}" alt="">{$user.aName}
            <span class=" fa fa-angle-down"></span>
          </a>
          <ul class="dropdown-menu dropdown-usermenu pull-right">
            <li><a href="/profile"> Profile</a></li>
            <li><a href="/login?out"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
          </ul>
        </li>
        <li>
          <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-transform: uppercase; cursor: pointer;">
          {$current_lang} <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li><a href="/interface?lang=tr">Türk</a></li>
            <li><a href="/interface?lang=ru">Русский</a></li>
            <li><a href="/interface?lang=en">English</a></li>
          </ul>
        </li>
        <li>
          <a style="min-width: 550px" class="pull-right"><span class="pull-left">{$_TRANS['your_link']}:&nbsp;</span><div class="input-group" style="margin: 0; max-width: 350px">
              <div class="input-group-addon"><i class="fa fa-group fa-fw"></i></div>
              <input type="text" id="referalUrl" name="ref-link" value="{$refurl}" class="form-control" onclick="select()" style="background: #ccc; border-top-right-radius: 3px; border-bottom-right-radius: 3px">
            </div></a>
         {*  <a>{$_TRANS['your_link']}: <strong>{$refurl}</strong></a> *}
        </li>
      </ul>

    </nav>
  </div>
</div>