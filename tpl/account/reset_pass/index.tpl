{strip}

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Alt Trade</title>

    <!-- Bootstrap -->
    <link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Ionicons -->
	<link rel="stylesheet" href="/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="/css/AdminLTE.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="/css/blue.css">

	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

	<script src="/vendors/jquery/dist/jquery.min.js"></script>
  </head>
  <body class="hold-transition login-page">
	<div class="login-box">
	<div class="login-logo">
			<a>{$_TRANS['Password reset']}</a>
		</div>

		<div class="login-box-body">

{if isset($smarty.get.done)}

	<h2>{$_TRANS['Operation completed']}!</h2>
	<p class="info">
		{$_TRANS['Now You can']} <a href="{_link module='account/login'}">{$_TRANS['login']}</a> {$_TRANS['to your account, with <b>new</b> password']|html_entity_decode}.<br>
		{$_TRANS['After that it must be changed to another']}
	</p>

{elseif isset($smarty.get.need_confirm)}

	<h2>{$_TRANS['Operation NOT completed']}!</h2>
	<p class="info">
		{$_TRANS['To get a temporary password']} <a href="{_link module='confirm'}">{$_TRANS['input confirmation code']}</a><br>
		{$_TRANS['or click on the link that was sent to your e-mail']}.<br><br>
		{$_TRANS['If message is not coming, then try']} <a href="{_link module='account/change_mail'}">{$_TRANS['change e-mail']}</a>
	</p>

{elseif isset($squest)}

	{include file='sqa.tpl'}

{else}

	<div class="block_form">
		{if $tpl_errors['account/reset_pass_frm']|count}
			<ul class="error_message">
		    	{$_TRANS['Main']}Please fix the following errors:<br/>
		        {if $tpl_errors['account/reset_pass_frm'][0]=='login_empty'}<li>{$_TRANS['Input Login/Password']}</li>{/if}
		        {if $tpl_errors['account/reset_pass_frm'][0]=='login_not_found'}<li>{$_TRANS['Wrong Login/Password']}</li>{/if}
		        {if $tpl_errors['account/reset_pass_frm'][0]=='mail_not_found'}<li>{$_TRANS['E-Mail not found']}</li>{/if}
		        {if $tpl_errors['account/reset_pass_frm'][0]=='captcha_wrong'}<li>{$_TRANS['Wrong code']}</li>{/if}
		    </ul>
		{/if}

		<form method="post" name="account/reset_pass_frm" {if $url} action="{if $url == '*'}{$_selfLink}{else}{$url}{/if}"{/if}>

			<div class="form-group">
		            <input name="Login" id="login_frm_Login" class="form-control" value="{$smarty.request.Login}" type="text" placeholder="{$_TRANS['Login']}">
		    </div>

			{if !$_cfg.Const_NoLogins}
	            <div class="form-group">
	            	<input name="Mail" class="form-control" id="login_frm_Mail" value="{$smarty.request.Mail}" type="text" placeholder="{$_TRANS['E-Mail']}">
	            </div>
	        {/if}

	        {_getFormSecurity form='account/reset_pass_frm' captcha=$_cfg.Account_ResetPassCaptcha}
	        {if $__Capt}
	        	{include file='captcha.tpl' form='account/reset_pass_frm' star=$edit_descr_star}
	        {/if}

	        <div class="block_form_el cfix">
	        	<label for="login_frm_Pass">&nbsp;</label>
	        	<div class="form-group">
	        		<input name="account/reset_pass_frm_btn" value="{$_TRANS['Next']}" type="submit" class="btn btn-primary btn-flat btn-block">
	            </div>
	        </div>
		</form>
	</div>

</div>
</div>
</body>
</html>

{/if}