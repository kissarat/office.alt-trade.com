{strip}
{if $_cfg.Account_Loginza}
	{include file='account/loginza/box.small.tpl'}
	<br>
	<h3>or</h3>
{/if}
	<form method="post" name="login_frm" action="login">


    <label for="login">Логин</label>
    <input type="text" class="style" id="login" name="Login">
    <label for="password">Пароль</label>
    <input type="password" class="style" id="password" name="Pass">
    <input type="checkbox" id="checkbox" name="Remember" value="1" class="checkbox" checked="checked">
    <label for="checkbox" class="checkbox-label">Запомнить меня</label>
<div class="text-danger">
    {if $tpl_errors['login_frm']|count}
    {if $tpl_errors['login_frm'][0]=='login_empty'}
        {$_TRANS['Input Login/Password']}
    {/if}
    {if $tpl_errors['login_frm'][0]=='login_not_found'}
        {$_TRANS['Wrong Login/Password']}
    {/if}
    {if $tpl_errors['login_frm'][0]=='not_active'}
        {$_TRANS['Account e-mail is not confirmed']}
    {/if}
    {if $tpl_errors['login_frm'][0]=='banned'}
        {$_TRANS['Access to the account is suspended']} {$ban_date}
    {/if}
    {if $tpl_errors['login_frm'][0]=='blocked'}
        {$_TRANS['Account is blocked']}
    {/if}
    {if $tpl_errors['login_frm'][0]=='captcha_wrong'}
        {$_TRANS['Wrong code']}
    {/if}
    {if $tpl_errors['login_frm'][0]=='GA_wrong'}
        {$_TRANS['GA_wrong_code']}
    {/if}
{/if}
</div>
{_getFormSecurity form='login_frm' captcha=$_cfg.Account_LoginCaptcha}
        {if $__Capt}
            {include file='captcha.tpl' form='login_frm' star=$edit_descr_star}
        {/if}
        {if $url}
            <input name="URL" id="login_frm_URL" value="{$url}" type="hidden">
        {/if}

        <input name="login_frm_btn" value="{$_TRANS['sign in']}" type="hidden">
        <div class="clearfix"></div>
        <div class="buttons">
            <div class="row">
                <div class="col-xs-6 col-md-7">
                    <input type="submit" value="Войти">
                </div>
                <div class="col-xs-6 col-md-5">
                    <a href="/registration" class="register">Регистрация</a>
                </div>
            </div>
        </div>
	</form>
{/strip}