{strip}

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Alt Trade</title>

    <!-- Bootstrap -->
    <link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Ionicons -->
	<link rel="stylesheet" href="/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="/css/AdminLTE.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="/css/blue.css">

	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

	<script src="/vendors/jquery/dist/jquery.min.js"></script>
  </head>

<body class="hold-transition login-page">
	<div class="login-box">
		{if isset($smarty.get.ip_changed)}

			<h2>{$_TRANS['Security system']}</h2>
			<p class="info">
				{$_TRANS['You are trying to access your account from a different IP-addresses']}.<br>
				{$_TRANS['To continue']} <a href="{_link module='confirm'}">{$_TRANS['input confirmation code']}</a><br> {$_TRANS['or click on the link that was sent to your e-mail']}
			</p>

		{elseif isset($smarty.get.brute_force)}

			<h2>{$_TRANS['Security system']}</h2>
			<p class="info">
				{$_TRANS['Password has been entered incorrectly multiple times']}.<br>
				{$_TRANS['To continue']} <a href="{_link module='confirm'}">{$_TRANS['input confirmation code']}</a><br>
				{$_TRANS['or click on the link that was sent to your e-mail']}
			</p>

		{else}
			{if $_cfg.Sys_LockSite}
				<p class="info">
					{$_TRANS['Currently on the site are technical works']}.<br>
					{$_TRANS['Login <b>only</b> for staff']|html_entity_decode}
				</p>
			{/if}

			{include file='account/login/box.tpl'}

		{/if}
	</div>

<!-- Bootstrap -->
<script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="/js/login.js"></script>
</body>
</html>

{/strip}