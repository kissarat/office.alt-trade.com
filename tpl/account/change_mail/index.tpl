{strip}
{include file='header.tpl'}


<div class="x_panel">
	<div class="x_title">
		<h2>{$_TRANS['Change e-mail']}</h2>
		<div class="clearfix"></div>
	</div>

	<div class="x_content">
		<ul class="nav nav-tabs bar_tabs">
			<li role="presentation"><a href="/profile">{$_TRANS['Profile']}</a></li>
		  <li role="presentation"><a href="/account">{$_TRANS['Security']}</a></li>
		  <li role="presentation" class="active"><a href="{_link module='account/change_mail'}">{$_TRANS['Change e-mail']}</a></li>
		  <li role="presentation"><a href="{_link module='account/change_pass'}">{$_TRANS['Change password']}</a></li>
		  <li role="presentation"><a href="/balance/wallets">Платежные реквизиты</a></li>
		</ul>

		<div class="col-md-6 col-xs-12">
			{if isset($smarty.get.done)}

			<h2>{$_TRANS['Е-mail changed!']}</h2>

			{elseif isset($smarty.get.need_confirm)}

				<h2>{$_TRANS['Operation NOT complete!']}</h2>
				<p class="info">
					{$_TRANS['To complete the operation, you must confirm your e-mail']}.<br>
					{$_TRANS['Please']} <a href="{_link module='confirm'}">{$_TRANS['input confirmation code']}</a><br>
					{$_TRANS['or click on the link that was sent to your e-mail']}.<br><br>
					{$_TRANS['If message is not coming, then try']} <a href="{_link module='account/change_mail'}">{$_TRANS['change e-mail']}</a>
				</p>

			{elseif isset($smarty.get.already_used)}

				<h2>{$_TRANS['The operation could not be completed!']}</h2>
				<p class="info">
					{$_TRANS['This e-mail can not be confirmed by you,<br>because it is already used by another user']|html_entity_decode}
				</p>

			{elseif isset($squest)}

				{include file='sqa.tpl'}

			{else}

				<div class="block_form">
					{if $tpl_errors['account/change_mail_frm']|count}
						<ul class="error_message">
					        {if $tpl_errors['account/change_mail_frm'][0]=='login_empty'}<li>{$_TRANS['Input Login/Password']}</li>{/if}
					        {if $tpl_errors['account/change_mail_frm'][0]=='login_not_found'}<li>{$_TRANS['Wrong Login/Password']}</li>{/if}
					        {if $tpl_errors['account/change_mail_frm'][0]=='not_active'}<li>{$_TRANS['E-Mail is not confirmed']}</li>{/if}
					        {if $tpl_errors['account/change_mail_frm'][0]=='banned'}<li>{$_TRANS['Access to the account is suspended']} {$ban_date}</li>{/if}
							{if $tpl_errors['account/change_mail_frm'][0]=='pass_not_found'}<li>{$_TRANS['Wrong Password']}</li>{/if}
					        {if $tpl_errors['account/change_mail_frm'][0]=='mail_empty'}<li>{$_TRANS['Input e-mail']}</li>{/if}
					        {if $tpl_errors['account/change_mail_frm'][0]=='mail_wrong'}<li>{$_TRANS['Wrong e-mail']}</li>{/if}
					        {if $tpl_errors['account/change_mail_frm'][0]=='mail_used'}<li>{$_TRANS['E-Mail is already used']}</li>{/if}
					        {if $tpl_errors['account/change_mail_frm'][0]=='captcha_wrong'}<li>{$_TRANS['Wrong code']}</li>{/if}
					    </ul>
					{/if}

					<form method="post" name="account/change_mail_frm" {if $url} action="{if $url == '*'}{$_selfLink}{else}{$url}{/if}"{/if}>

						{if !_uid()}
							<div class="block_form_el cfix">
								{$txt_login=valueIf($_cfg.Const_NoLogins, 'e-mail', $_TRANS['Login'])}
					            <label for="login_frm_Login">{$txt_login} <span class="descr_star">*</span></label>
					            <div class="block_form_el_right">
					            	<input name="Login" id="login_frm_Login" value="{$smarty.request.Login}" type="text" class="form-control">
					            </div>
					        </div>
				        {/if}

				        <div class="form-group">
				            <label for="login_frm_Pass">{$_TRANS['Password']} <span class="descr_star">*</span></label>
				            <div class="block_form_el_right">
				            	<input name="Pass" id="login_frm_Pass" value="" type="password" class="form-control">
				            </div>
				        </div>

				        <div class="form-group">
				            <label for="login_frm_NewMail">{valueIf($_cfg.Account_ChangeMailConfirm, {$_TRANS['New e-mail (confirmation will be sent)']}, $_TRANS['New e-mail'])} <span class="descr_star">*</span></label>
				            <div class="block_form_el_right">
				            	<input name="NewMail" id="login_frm_NewMail" value="{$smarty.request.NewMail}" type="text" class="form-control">
				            </div>
				        </div>

				        {_getFormSecurity form='account/change_mail_frm' captcha=$_cfg.Account_ChangeMailCaptcha}
				        {if $__Capt}
				        	{include file='captcha.tpl' form='account/change_mail_frm' star=$edit_descr_star}
				        {/if}

				        <div class="block_form_el cfix">
				        	<label for="login_frm_Pass">&nbsp;</label>
				        	<div class="block_form_el_right">
				        		<input name="account/change_mail_frm_btn" value="{$_TRANS['Next']}" type="submit" class="button-green btn btn-info">
				            </div>
				        </div>
					</form>
				</div>

			{/if}
		</div>

	</div>
</div>

{include file='footer.tpl' class="cabinet"}
{/strip}