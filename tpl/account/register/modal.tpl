{strip}



<h1><span>Р</span>егистрация</h1>

{if isset($smarty.get.done)}

	<h2>{$_TRANS['Registration complete']}!</h2>
	<p class="info">
		{$_TRANS['Now You can']} <a href="{_link module='account/login'}">{$_TRANS['login']}</a> {$_TRANS['to your account']}
	</p>

{elseif isset($smarty.get.need_confirm)}

	<h2>{$_TRANS['Registration']}!</h2>
	<p class="info">
		{$_TRANS['To complete the operation, you must confirm your e-mail']}.<br>
		{$_TRANS['Please']} <a href="{_link module='confirm'}">{$_TRANS['input confirmation code']}</a><br>
		{$_TRANS['or click on the link that was sent to your e-mail']}.<br><br>
		{$_TRANS['If message is not coming, then try']} <a href="{_link module='account/change_mail'}">{$_TRANS['change e-mail']}</a>
	</p>

{elseif isset($smarty.get.need_confirm_sms)}

	<h2>{$_TRANS['Registration NOT complete']}!</h2>
	<p class="info">
		{$_TRANS['To complete the operation, you must confirm your phone number']}.<br>
		{$_TRANS['Please']} <a href="{_link module='confirm'}">{$_TRANS['input confirmation code']}</a><br>
		{$_TRANS['that was sent to your phone']}
	</p>

{elseif $_cfg.Account_RegMode == 0}

	<h2>{$_TRANS['Registration suspended']}!</h2>
	<p class="info">
		{$_TRANS['Registration on the site is temporarily suspended']}.<br>
		{$_TRANS['For all questions please contact the']} <a href="{_link module='message/support'}">{$_TRANS['support']}</a>
	</p>

{else}

	{$txt_login=valueIf($_cfg.Const_NoLogins, 'e-mail', $_TRANS['login'])}
	{if ($_cfg.Account_RegMode == 2) and !$valid_ref}

		<h2>{$_TRANS['Attention']}!</h2>
		<p class="info">
			{$_TRANS['Registration on the site is possible']} <a href="{_link module='udp/about'}">{$_TRANS['by invitation']}</a> <b>{$_TRANS['only']}</b>.<br>
			{$_TRANS['To do this, you must come to our website<br>by special member ref-link or specify the']|html_entity_decode} {$txt_login}
		</p>

	{elseif $_cfg.Account_RegMode == 3}

		<h2>{$_TRANS['Attention']}!</h2>
		<p class="info">
			{$_TRANS['Registration on the site is possible']} <a href="{_link module='udp/about'}">{$_TRANS['by invitation']}</a> <b>{$_TRANS['only']}</b>.<br>
			{$_TRANS['To do this, you must specify an invitation code']}
		</p>

	{/if}

	{if $_cfg.Account_Loginza and (($_cfg.Account_RegMode == 1) or (($_cfg.Account_RegMode == 2) and $valid_ref)) and ($_cfg.Sec_MinSQA == 0)}
		{include file='account/loginza/box.small.tpl'}
		<br>
		<h3>{$_TRANS['or']}</h3>
	{/if}

	<div class="block_form row">
		<form method="post" action="registration" name="register_frm">

			{if $_cfg.Account_UseName == 1}
				<div class="col-md-6">
					<label for="name">{$_TRANS['You name']}</label>
					<input type="text" id="name" name="aName" value="{$smarty.post.aName}">
				</div>
	        {/if}

	        {if !$_cfg.Const_NoLogins}
	        	<div class="col-md-6">
	        		<label for="login">{$_TRANS['Login']}</label>
					<input type="text" id="login" name="uLogin" value="{$smarty.post.uLogin}">
				</div>
	        {/if}
	        	<div class="col-md-6">
	        		<label for="email">{valueIf($_cfg.Account_RegConfirm, $_TRANS['E-Mail (confirmation will be sent)'], 'E-Mail')}</label>
					<input type="email" id="email" name="uMail" value="{$smarty.post.uMail}">
				</div>
				
				<div class="col-md-6">
					<label for="password">{$_TRANS['Password']}</label>
					<input type="password" id="password" name="uPass">
				</div>

				<div class="col-md-6">
					<label for="repassword">{$_TRANS['Repeat password']}</label>
					<input type="password" id="repassword" name="Pass2">
				</div>

			{if $_cfg.SMS_REG}
				<div class="col-md-6">
					<label for="tel">{$_TRANS['Phone number (with country code)']}</label>
					<input name="aTel" id="tel" value="{$smarty.post.aTel}" type="tel">
				</div>
	        {/if}

			<div class="col-md-6 hidden">
				<label for="register_frm_aDefCurr">{$_TRANS['Default payment system']}</label>
				<select name="aDefCurr">
					{foreach from=$currs2 item=c key=k}
	                <option value="{$k}"{if $smarty.post.aDefCurr == $k} selected{/if}>{$c.cName}</option>
	              {/foreach}
				</select>
			</div>
		
	        {if (($_cfg.Account_RegMode != 3) or $_cfg.Ref_Word)}
		        <div class="col-md-12">
		        	<div class="block_form_el cfix">
			            <label for="register_frm_uRef">{$_TRANS['You invited by']}</label>
			            <div class="block_form_el_right">
	                        {$smarty.session._ref}
			            	<input name="uRef" id="register_frm_uRef" value="{$smarty.session._ref}" type="hidden"/>
			            </div>
			        </div>
			    </div>
	        {/if}

	        {if ($_cfg.Account_RegMode == 3)}
		        <div class="col-md-6">
		        	<div class="block_form_el cfix">
			            <label for="register_frm_Invite">{$_TRANS['Invite code']}</label>
			            <div class="block_form_el_right">
			            	<input name="Invite" id="register_frm_Invite" value="{$smarty.post.Invite}" type="text">
			            </div>
			        </div>
			    </div>
	        {/if}

	        {if ($_cfg.Sec_MinSQA != 0)}
		        <div class="col-md-6">
		        	<div class="block_form_el cfix">
			            <label for="register_frm_aSQuestion">{$_TRANS['Secret question']} <span class="descr_star">*</span></label>
			            <div class="block_form_el_right">
			            	<input name="aSQuestion" id="register_frm_aSQuestion" value="{$smarty.post.aSQuestion}" type="text">
			            </div>
			        </div>
			    </div>
	        {/if}

	        {if ($_cfg.Sec_MinSQA != 0)}
		        <div class="col-md-6">
		        	<div class="block_form_el cfix">
			            <label for="register_frm_aSAnswer">{$_TRANS['Secret answer']} <span class="descr_star">*</span></label>
			            <div class="block_form_el_right">
			            	<input name="aSAnswer" id="register_frm_aSAnswer" value="{$smarty.post.aSAnswer}" type="text">
			            </div>
			        </div>
			    </div>
	        {/if}

	        <div class="clearfix"></div>

	        <div class="col-md-12">
	        	<input name="Agree" id="checkbox" type="checkbox" class="checkbox" value="1">
    			<label for="checkbox" class="checkbox-label">Принять
    				<a href="rules"> условия</a>
    			</label>
	        </div>

	        <div class="clearfix"></div>

            {_getFormSecurity form='register_frm' captcha=$_cfg.Account_RegCaptcha}
            {if $__Capt}
	        	{include file='captcha.tpl' form='register_frm' star=$edit_descr_star}
	        {/if}
			
            <div class="col-md-12">
                <input type="submit" name="register_frm_btn" value="{$_TRANS['Sign up']}">
            </div>
        </form>
    </div>

{/if}
{/strip}