{strip}
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Alt Trade</title>

    <!-- Bootstrap -->
    <link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Ionicons -->
	<link rel="stylesheet" href="/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="/css/AdminLTE.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="/css/blue.css">

	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

	<script src="/vendors/jquery/dist/jquery.min.js"></script>
  </head>

  <body class="hold-transition login-page">

	<div class="login-box">

		<div class="login-logo">
			<a><b>Create</b> Account</a>
		</div>

		<div class="login-box-body">

			<p class="login-box-msg">{$_TRANS['Register a new membership']}</p>

{if isset($smarty.get.done)}

	<h2>{$_TRANS['Registration complete']}!</h2>
	<p class="info">
		{$_TRANS['Now You can']} <a href="{_link module='account/login'}">{$_TRANS['login']}</a> {$_TRANS['to your account']}
	</p>

{elseif isset($smarty.get.need_confirm)}

	<h2>{$_TRANS['Registration']}!</h2>
	<p class="info">
		{$_TRANS['To complete the operation, you must confirm your e-mail']}.<br>
		{$_TRANS['Please']} <a href="{_link module='confirm'}">{$_TRANS['input confirmation code']}</a><br>
		{$_TRANS['or click on the link that was sent to your e-mail']}.<br><br>
		{$_TRANS['If message is not coming, then try']} <a href="{_link module='account/change_mail'}">{$_TRANS['change e-mail']}</a>
	</p>

{elseif isset($smarty.get.need_confirm_sms)}

	<h2>{$_TRANS['Registration NOT complete']}!</h2>
	<p class="info">
		{$_TRANS['To complete the operation, you must confirm your phone number']}.<br>
		{$_TRANS['Please']} <a href="{_link module='confirm'}">{$_TRANS['input confirmation code']}</a><br>
		{$_TRANS['that was sent to your phone']}
	</p>

{elseif $_cfg.Account_RegMode == 0}

	<h2>{$_TRANS['Registration suspended']}!</h2>
	<p class="info">
		{$_TRANS['Registration on the site is temporarily suspended']}.<br>
		{$_TRANS['For all questions please contact the']} <a href="{_link module='message/support'}">{$_TRANS['support']}</a>
	</p>

{else}

	{$txt_login=valueIf($_cfg.Const_NoLogins, 'e-mail', $_TRANS['login'])}
	{if ($_cfg.Account_RegMode == 2) and !$valid_ref}

		<h2>{$_TRANS['Attention']}!</h2>
		<p class="info">
			{$_TRANS['Registration on the site is possible']} <a href="{_link module='udp/about'}">{$_TRANS['by invitation']}</a> <b>{$_TRANS['only']}</b>.<br>
			{$_TRANS['To do this, you must come to our website<br>by special member ref-link or specify the']|html_entity_decode} {$txt_login}
		</p>

	{elseif $_cfg.Account_RegMode == 3}

		<h2>{$_TRANS['Attention']}!</h2>
		<p class="info">
			{$_TRANS['Registration on the site is possible']} <a href="{_link module='udp/about'}">{$_TRANS['by invitation']}</a> <b>{$_TRANS['only']}</b>.<br>
			{$_TRANS['To do this, you must specify an invitation code']}
		</p>

	{/if}

	{if $_cfg.Account_Loginza and (($_cfg.Account_RegMode == 1) or (($_cfg.Account_RegMode == 2) and $valid_ref)) and ($_cfg.Sec_MinSQA == 0)}
		{include file='account/loginza/box.small.tpl'}
		<br>
		<h3>{$_TRANS['or']}</h3>
	{/if}

	<div class="block_form">
		<form method="post" action="registration" name="register_frm">
            {if $tpl_errors|count}
	        	<ul class="error_message text-danger">
	            	{$_TRANS['Please fix the following errors']}:<br/>
	                {if $tpl_errors['register_frm'][0]=='name_empty'}<li>{$_TRANS['Input Name']}</li>{/if}
	                {if $tpl_errors['register_frm'][0]=='login_empty'}<li>{$_TRANS['Enter login']}</li>{/if}
	                {if $tpl_errors['register_frm'][0]=='login_short'}<li>{$_TRANS['Login is too short, less than']} {$_cfg.Account_MinLogin}</li>{/if}
	                {if $tpl_errors['register_frm'][0]=='login_wrong'}<li>{$_TRANS['Wrong login format']}</li>{/if}
	                {if $tpl_errors['register_frm'][0]=='login_used'}<li>{$_TRANS['Registration']}</li>{/if}
	                {if $tpl_errors['register_frm'][0]=='mail_empty'}<li>{$_TRANS['Input e-mail']}</li>{/if}
	                {if $tpl_errors['register_frm'][0]=='mail_wrong'}<li>{$_TRANS['Wrong e-mail']}</li>{/if}
	                {if $tpl_errors['register_frm'][0]=='mail_used'}<li>{$_TRANS['E-Mail is already used']}</li>{/if}
	                {if $tpl_errors['register_frm'][0]=='pass_empty'}<li>{$_TRANS['Input password']}</li>{/if}
	                {if $tpl_errors['register_frm'][0]=='pass_short'}<li>{$_TRANS['Password is too short, less than']} {$_cfg.Account_MinPass}</li>{/if}
	                {if $tpl_errors['register_frm'][0]=='pass_wrong'}<li>{$_TRANS['Password does not match the format']}</li>{/if}
	                {if $tpl_errors['register_frm'][0]=='pass_not_equal'}<li>{$_TRANS['Passwords do not match']}</li>{/if}
	                {if $tpl_errors['register_frm'][0]=='tel_wrong'}<li>{$_TRANS['Wrong number']}</li>{/if}
					{if $tpl_errors['register_frm'][0]=='defpsys_wrong'}<li>{$_TRANS['Wrong default payment system']}</li>{/if}
	                {if $tpl_errors['register_frm'][0]=='ref_empty'}<li>{$_TRANS['Input']} {$txt_login}</li>{/if}
	                {if $tpl_errors['register_frm'][0]=='ref_not_found'}<li>{$txt_login} {$_TRANS['not found']}</li>{/if}
	                {if $tpl_errors['register_frm'][0]=='inv_empty'}<li>{$_TRANS['Input code']}</li>{/if}
	                {if $tpl_errors['register_frm'][0]=='inv_not_found'}<li>{$_TRANS['Wrong code']}</li>{/if}
	                {if $tpl_errors['register_frm'][0]=='inv_used'}<li>{$_TRANS['Code is already in use']}</li>{/if}
					{if $tpl_errors['register_frm'][0]=='secq_empty'}<li>{$_TRANS['Input question']}</li>{/if}
					{if $tpl_errors['register_frm'][0]=='secq_short'}<li>{$_TRANS['question is too short']} ({$_TRANS['less than']} {$_cfg.Sec_MinSQA})</li>{/if}
					{if $tpl_errors['register_frm'][0]=='seca_empty'}<li>{$_TRANS['Input answer']}</li>{/if}
					{if $tpl_errors['register_frm'][0]=='seca_short'}<li>{$_TRANS['answer is too short']} ({$_TRANS['less than']} {$_cfg.Sec_MinSQA})</li>{/if}
					{if $tpl_errors['register_frm'][0]=='seqa_equal_secq'}<li>{$_TRANS['Answer can not be the same with the question']}</li>{/if}
					{if $tpl_errors['register_frm'][0]=='captcha_wrong'}<li>{$_TRANS['Wrong code']}</li>{/if}
	                {if $tpl_errors['register_frm'][0]=='must_agree'}<li>{$_TRANS['You must accept the rules']}</li>{/if}
	            </ul>
	        {/if}

			{if $_cfg.Account_UseName == 1}
				<div class="form-group has-feedback has-feedback-left">
					<input type="text" name="aName" value="{$smarty.post.aName}" class="form-control" placeholder="{$_TRANS['You name']}">
					<div class="form-control-feedback">
						<i class="icon-user-check text-muted"></i>
					</div>
				</div>
	        {/if}

	        {if !$_cfg.Const_NoLogins}
	        	<div class="form-group has-feedback has-feedback-left">
					<input type="text" name="uLogin" value="{$smarty.post.uLogin}" class="form-control" placeholder="{$_TRANS['Login']}">
					<div class="form-control-feedback">
						<i class="icon-user-check text-muted"></i>
					</div>
				</div>
	        {/if}

	        <div class="form-group has-feedback has-feedback-left">
				<input type="email" name="uMail" class="form-control" placeholder="{valueIf($_cfg.Account_RegConfirm, $_TRANS['E-Mail (confirmation will be sent)'], 'E-Mail')}" value="{$smarty.post.uMail}">
				<div class="form-control-feedback">
					<i class="icon-mention text-muted"></i>
				</div>
			</div>

			<div class="form-group has-feedback has-feedback-left">
				<input type="password" name="uPass" class="form-control" placeholder="{$_TRANS['Password']}">
				<div class="form-control-feedback">
					<i class="icon-user-lock text-muted"></i>
				</div>
			</div>
			<div class="form-group has-feedback has-feedback-left">
				<input type="password" name="Pass2" class="form-control" placeholder="{$_TRANS['Repeat password']}">
				<div class="form-control-feedback">
					<i class="icon-user-lock text-muted"></i>
				</div>
			</div>

			{if $_cfg.SMS_REG}
				<div class="form-group has-feedback has-feedback-left">
					<input name="aTel" class="form-control" value="{$smarty.post.aTel}" type="text" placeholder="{$_TRANS['Phone number (with country code)']}">
					<div class="form-control-feedback">
						<i class="icon-user-check text-muted"></i>
					</div>
				</div>
	        {/if}

	        <div class="form-group" style="display: none;">
	          <label for="register_frm_aDefCurr" class="text-left pull-left">{$_TRANS['Default payment system']}</label>
	            <select name="aDefCurr" class="form-control">
	              {foreach from=$currs2 item=c key=k}
	                <option value="{$k}"{if $smarty.post.aDefCurr == $k} selected{/if}>{$c.cName}</option>
	              {/foreach}
	            </select>
	        </div>
			{if ($smarty.session._ref)}
				{if (($_cfg.Account_RegMode != 3) or $_cfg.Ref_Word)}
			        <div class="block_form_el cfix">
			            <label for="register_frm_uRef" class="pull-left">{$_TRANS['You invited by']}:&nbsp;</label>
			            <div class="block_form_el_right">
	                        {$smarty.session._ref}
			            	<input name="uRef" id="register_frm_uRef" value="{$smarty.session._ref}" type="hidden"/>
			            </div>
			        </div>
			        <br>
		        {/if}
			{/if}
	        

	        {if ($_cfg.Account_RegMode == 3)}
		        <div class="block_form_el cfix">
		            <label for="register_frm_Invite">{$_TRANS['Invite code']}</label>
		            <div class="block_form_el_right">
		            	<input name="Invite" id="register_frm_Invite" value="{$smarty.post.Invite}" type="text">
		            </div>
		        </div>
	        {/if}

	        {if ($_cfg.Sec_MinSQA != 0)}
		        <div class="block_form_el cfix">
		            <label for="register_frm_aSQuestion">{$_TRANS['Secret question']} <span class="descr_star">*</span></label>
		            <div class="block_form_el_right">
		            	<input name="aSQuestion" id="register_frm_aSQuestion" value="{$smarty.post.aSQuestion}" type="text">
		            </div>
		        </div>
	        {/if}

	        {if ($_cfg.Sec_MinSQA != 0)}
		        <div class="block_form_el cfix">
		            <label for="register_frm_aSAnswer">{$_TRANS['Secret answer']} <span class="descr_star">*</span></label>
		            <div class="block_form_el_right">
		            	<input name="aSAnswer" id="register_frm_aSAnswer" value="{$smarty.post.aSAnswer}" type="text">
		            </div>
		        </div>
	        {/if}

	        <div class="row">
				<div class="col-xs-8">
					<div class="checkbox">
						<label>
							<input name="Agree" type="checkbox" value="1" class="styled">
							&nbsp;Принять <a href="rules">условия</a>
						</label>
					</div>					
				</div>

				<div class="col-xs-4">
					<button type="submit" name="register_frm_btn" class="btn btn-primary btn-flat btn-block">{$_TRANS['Sign up']}</button>
				</div>
			</div>

            {_getFormSecurity form='register_frm' captcha=$_cfg.Account_RegCaptcha}
            {if $__Capt}
	        	{include file='captcha.tpl' form='register_frm' star=$edit_descr_star}
	        {/if}
        </form>
    </div>

{/if}

{if !$_cfg.Const_NoLogins}

	<script type="text/javascript">
		function chkLogin()
		{
			$('#register_frm_uLogin').removeClass('error');
			$('#login_check').html('');
			$.ajax(
				{
					type: 'POST',
					url: 'ajax',
					data: 'module=account/register&do=chklogin&login='+
						encodeURIComponent($('#register_frm_uLogin').val()),
					success:
						function(ex)
						{
							if (ex == 1)
							{
								$('#register_frm_uLogin').addClass('error');
								$('#login_check').html('used');
							}
						}
				}
			);
		}
		tid2=0;
		$('#register_frm_uLogin').keypress(
			function()
			{
				clearTimeout(tid2);
				tid2=setTimeout(function(){ chkLogin(); }, 1000);
			}
		);
		chkLogin();
	</script>

{/if}

	<script type="text/javascript">
		function chkMail()
		{
			$('#register_frm_uMail').removeClass('error');
			$('#mail_check').html('');
			$.ajax(
				{
					type: 'POST',
					url: 'ajax',
					data: 'module=account/register&do=chkmail&mail='+
						encodeURIComponent($('#register_frm_uMail').val()),
					success:
						function(ex)
						{
							if (ex == 1)
							{
								$('#register_frm_uMail').addClass('error');
								$('#mail_check').html('used');
							}
						}
				}
			);
		}
		tid=0;
		$('#register_frm_uMail').keypress(
			function()
			{
				clearTimeout(tid);
				tid=setTimeout(function(){ chkMail(); }, 1000);
			}
		);
		chkMail();
	</script>


          </section>
        </div>
      </div>
    </div>

  </body>
</html>

<!-- Bootstrap -->
<script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
{* <!-- iCheck -->
<script src="/js/icheck.min.js"></script> *}
{* 
<script src="/js/login.js"></script> *}
{/strip}