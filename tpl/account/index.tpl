{strip}
{include file='header.tpl'}

<div class="x_panel">
	<div class="x_title">
		<h2>{$_TRANS['Security']}</h2>
		<div class="clearfix"></div>
	</div>

	<div class="x_content">

		<ul class="nav nav-tabs bar_tabs">
			<li role="presentation"><a href="/profile">{$_TRANS['Profile']}</a></li>
		  <li role="presentation" class="active"><a href="/account">{$_TRANS['Security']}</a></li>
		  <li role="presentation"><a href="{_link module='account/change_pass'}">{$_TRANS['Change password']}</a></li>
		  <li role="presentation"><a href="/balance/wallets">Платежные реквизиты</a></li>
		</ul>

		<div class="col-md-6 col-xs-12">
		
			<div>
				{if $tpl_errors|count}
					<ul class="error_message">
					    {if $tpl_errors['account_frm'][0]=='name_empty'}<li>{$_TRANS['Input name']}</li>{/if}
					    {if $tpl_errors['account_frm'][0]=='tel_wrong'}<li>{$_TRANS['Wrong number']}></li>{/if}
					    {if $tpl_errors['account_frm'][0]=='tz_wrong'}<li>{$_TRANS['Wrong zone (h:m)']}</li>{/if}
					    {if $tpl_errors['account_frm'][0]=='secq_empty'}<li>{$_TRANS['Input question']}</li>{/if}
					    {if $tpl_errors['account_frm'][0]=='secq_short'}<li>{$_TRANS['Question is too short']} ({$_TRANS['less than']} {$_cfg.Sec_MinSQA})</li>{/if}
					    {if $tpl_errors['account_frm'][0]=='seca_empty'}<li>{$_TRANS['Input answer']}</li>{/if}
					    {if $tpl_errors['account_frm'][0]=='seca_short'}<li>{$_TRANS['Answer is too short']} ({$_TRANS['less than']} {$_cfg.Sec_MinSQA})</li>{/if}
					    {if $tpl_errors['account_frm'][0]=='seqa_equal_secq'}<li>{$_TRANS['Answer can not be the same with the question']}</li>{/if}
					    {if $tpl_errors['account_frm'][0]=='pin_wrong'}<li>{$_TRANS['Wrong code']}</li>{/if}
					    {if $tpl_errors['account_frm'][0]=='ga_wrong'}<li>{$_TRANS['GA_wrong_code']}</li>{/if}
					</ul>
				{/if}

				<form method="post" action="account" name="account_frm">
							<div class="form-group">
								<label for="">{$_TRANS['Control IP-address change']}</label>
								<div class="block_form_el_right">
									<select name="aIPSec" id="account_frm_aIPSec" class="form-control">
										<option value="0">- {$_TRANS['default']} -</option>
										<option value="1" {if $user.aIPSec==1}selected="selected"{/if}>x.0.0.0</option>
										<option value="2" {if $user.aIPSec==2}selected="selected"{/if}>x.x.0.0</option>
										<option value="3" {if $user.aIPSec==3}selected="selected"{/if}>x.x.x.0</option>
										<option value="4" {if $user.aIPSec==4}selected="selected"{/if}>x.x.x.x</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="checkbox">
									<label for="login_frm_Pass">
										<input name="aSessIP" id="account_frm_aSessIP" value="1" {if $user.aSessIP}checked="checked"{/if} type="checkbox">
							        	{$_TRANS['Bind session to IP-address']}
									</label>
								</div>
								<div class="checkbox">
									<label for="login_frm_Pass">
										<input name="aSessUniq" id="account_frm_aSessUniq" value="1" {if $user.aSessUniq}checked="checked"{/if} type="checkbox">
										{$_TRANS['Disallow parallel sessions']}
									</label>
								</div>
								{if !$_cfg.Msg_NoMail}
									<div class="checkbox">
										<label for="login_frm_Pass">
							            	<input name="aNoMail" id="account_frm_aNoMail" value="1" {if $user.aNoMail}checked="checked"{/if} type="checkbox">
											{$_TRANS['Do not be notified to e-mail']}
										</label>
									</div>
								{/if}
							</div>
							<div class="form-group">
								<label for="">{$_TRANS['Auto logout in N minutes (0 - default)']}</label>
								<div class="block_form_el_right">
									<input name="aTimeOut" id="account_frm_aTimeOut" value="{$user.aTimeOut}" type="text" class="form-control">
								</div>
							</div>
							{if ($_cfg.Sec_MinSQA != 0)}
								<div class="form-group">
									<label for="">{$_TRANS['Secret question']}</label>
									<div class="block_form_el_right">
										<textarea class="ckeditor form-control" name="aSQuestion" id="account_frm_aSQuestion" value="{$user.aSQuestion}" type="text" ></textarea>
									</div>
								</div>
							{/if}
							{if ($_cfg.Sec_MinSQA != 0)}
								<div class="form-group">
									<label for="">{$_TRANS['Secret answer(input to change)']}</label>
									<div class="block_form_el_right">
										<input name="aSAnswer" id="account_frm_aSAnswer" value="{$user.aSAnswer}" type="text" class="form-control">
									</div>
								</div>
							{/if}
							{if ($_cfg.Sec_MinPIN != 0)}
								<div class="form-group">
									<label for="">{$_TRANS['Input PIN-code (to confirm changes)']}</label>
									<div class="block_form_el_right">
										<input name="PIN" id="account_frm_PIN" value="{$user.PIN}" type="text" class="form-control">
									</div>
								</div>
							{/if}
					{_getFormSecurity form='account_frm'}
					<br><input name="account_frm_btn" value="{$_TRANS['Save']}" type="submit" class="btn btn-info">
				</form>
			</div>
		</div>
	</div>
</div>
{include file='footer.tpl' class="cabinet"}

{/strip}