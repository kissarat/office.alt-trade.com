{strip}
        <div class="clearfix"></div>
		</div>
      </div>
    </div>

    <!-- Bootstrap -->
    <script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/vendors/nprogress/nprogress.js"></script>

    <script src="/vendors/bootstrap-fileinput/js/fileinput.min.js"></script>
    <script src="/vendors/bootstrap-fileinput/js/locales/ru.js"></script>

    <script src="/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="/build/js/custom.min.js"></script>

    <script src="/js/main.js"></script>

    <script src="/js/jivosite.js"></script>
  </body>
</html>
{/strip}