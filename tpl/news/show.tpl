{strip}
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title></title>
	
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/jquery.bxslider.css">
    <link rel="stylesheet" href="/css/lazyYT.css">
    <link rel="stylesheet" href="/css/main.css">

  </head>
  <body>
    <header>
        <canvas id="spiders" class="hidden-xs spiders"></canvas>
    	<div class="container">
            <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-navbar" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="logo pull-left">
                <a href="/"><img src="/images/logo.png" alt=""></a>
            </div>
            </div>
    		<nav class="pull-right collapse navbar-collapse" id="collapse-navbar">
    			<ul class="lang">
    				<li><a href="interface?lang=ru"><img src="/images/ru.png"></a></li>
    				<li><a href="interface?lang=en"><img src="/images/en.png"></a></li>
    			</ul>
    			<div class="clearfix"></div>
    			<ul class="navigation">
    				<li><a href="">о компании</a></li>
    				<li><a href="">инвесторам</a></li>
    				<li><a href="">партнерам</a></li>
    				<li><a href="">контакты</a></li>
    				<li><a href="cabinet">кабинет</a></li>
    			</ul>
    		</nav>
    		<div class="clearfix"></div>

    		<div class="page-header">
    			<h1>зарабатывайте от 30% в месяц на крипторынке<br>
					<span>сделайте  вашу  мечту реальной</span>
				</h1>
    		</div>

    		<div class="login">
    			{include file='modal-register.tpl'}
                {include file='modal-login.tpl'}
    		</div>
    	</div>
    </header>
        <div class="container">
            <div class="media about-article pull-right col-md-11">
                <div class="media-left media-middle">
                    <p>{$el.nTopic}</p>
                </div>
                <div class="media-body">
                    {$el.nText}
                </div>
            </div>
        </div>

    <footer>
    	<div class="container">
    		<div class="row">
    			<div class="col-md-6 footer-contacts">
    				<img src="/images/logo.png" alt="">
    				<h4>Наши контакты</h4>
    				<a href=""><img src="/images/mail-foot.png" alt="">&nbsp;vk.com/logo.business</a>
    			</div>
    			<div class="col-md-6 footer-social">
    				<p>мы в социальных сетях</p>
    				<a href=""><img src="/images/vk-foot.png" alt=""></a>
    				<a href=""><img src="/images/facebook-foot.png" alt=""></a>
    				<a href=""><img src="/images/youtube-foot.png" alt=""></a>
    			</div>
    		</div>
    	</div>
		<div class="footer-nav">
			<div class="container">
				<div class="copy"><img src="/images/copyright.png" alt=""><span>L</span>ogo business</div>
				<ul class="navigation">
					<li><a href="">о компании</a></li>
					<li><a href="">инвесторам</a></li>
					<li><a href="">партнерам</a></li>
					<li><a href="">контакты</a></li>
					<li><a href="/cabinet">кабинет</a></li>
				</ul>
			</div>
		</div>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery.fitvids.js"></script>
    <script src="/js/jquery.bxslider.min.js"></script>
    <script src="/js/lazyYT.js"></script>
    <script src="/js/nodes.js"></script>
    <script src="/js/main.js"></script>
  </body>
</html>
{/strip}