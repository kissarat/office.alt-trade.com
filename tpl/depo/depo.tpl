{strip}
{include file='header.tpl' title=$_TRANS['Deposit'] class="cabinet"}
<div class="row balance">
    <div class="col-md-6">
        <div class="x_panel">
            
{if $el}
<div class="x_content">
<div class="x_title">
    <h2>{$_TRANS['Deposit']}</h2>
    <div class="clearfix"></div>
</div>
    {include file='depo/_depo.tpl'}

{else}

	{include file='balance/_bal.tpl'}
<div class="x_content">
<div class="x_title">
    <h2>{$_TRANS['New deposit']}</h2>
    <div class="clearfix"></div>
</div>
    {include file='depo/_depo.interval.tpl'}
    {include file='depo/_depo.new.tpl'}

{/if}

            </div>
        </div>
    </div>
</div>

{include file='footer.tpl' class="cabinet"}
{/strip}