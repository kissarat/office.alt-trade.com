{strip}
<td class="nowrap">
	<small>{$l.dCTS}</small>
</td>
<td align="right">
	{_z($l.dZD, $l.dcCurrID, 1)}
</td>
<td>
	{$l.pName}
</td>
<td class="nowrap">
	<small>{$l.dLTS}</small>
</td>
<td>
	{$l.dNPer} из {$l.pNPer}
</td>
<td align="right">
	{_z($l.dZP, $l.dcCurrID, 1)}
</td>
<td class="nowrap">
	<small>{$l.dNTS}</small>
</td>
<td>
	{$ststrs[$l.dState]}
</td>
{/strip}