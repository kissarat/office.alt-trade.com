{strip}

{include file='depo/_statuses.tpl'}

<div class="content panel panel-flat">
	<br>

{$chg=(($el.dState == 1) and ($el.pCompndMax > 0) and ($el.pCompndMin < $el.pCompndMax))}
{$modbuttons=[] scope='global'}
{if $chg}
	{$modbuttons['chg']='Change compaund' scope='global'}
{/if}

{$add=$el.pEnAdd}
{$sub=(($el.pClComis < 100) and ($el.dNPer >= $el.pMPer))}
{$modform=[] scope='global'}
{if ($el.dState == 1) and ($add or $sub)}
	{$modform=[
		1=>'',
		'Sum'=>
			[
				'$',
				'Сумма изменения!!',
				[
					'sum_empty'=>'укажите сумму',
					'sum_wrong'=>'неверная сумма',
					'low_bal1'=>'недостаточно средств',
					'cant_add'=>'невозможно довложить',
					'cant_sub'=>'невозможно снять',
					'plan_not_found'=>'нет подходящего плана'
				],
				'comment'=>" <i><small>{$el.cCurr}</small></i>",
				'default'=>0
			]
	] scope='global'}
	{if $add}
		{$modbuttons['add']='Add' scope='global'}
	{/if}
	{if $sub}
		{$modbuttons['sub']='Sub' scope='global'}
	{/if}
{/if}


<div class="block_form">
    {if $tpl_errors['depo/depo_frm']|count}
        <ul class="error_message">
            {$_TRANS['Please fix the following errors']}:<br/>
            {if $tpl_errors['depo/depo_frm'][0]=='compnd_wrong'}<li>{$_TRANS['Wrong value']}</li>{/if}
            {if $tpl_errors['depo/depo_frm'][0]=='sum_empty'}<li>{$_TRANS['Input amount']}</li>{/if}
            {if $tpl_errors['depo/depo_frm'][0]=='sum_wrong'}<li>{$_TRANS['Wrong amount']}</li>{/if}
            {if $tpl_errors['depo/depo_frm'][0]=='low_bal1'}<li>{$_TRANS['Insufficient funds']}</li>{/if}
            {if $tpl_errors['depo/depo_frm'][0]=='cant_add'}<li>{$_TRANS['Can`t add']}</li>{/if}
            {if $tpl_errors['depo/depo_frm'][0]=='cant_sub'}<li>{$_TRANS['Can`t sub']}</li>{/if}
            {if $tpl_errors['depo/depo_frm'][0]=='plan_not_found'}<li>{$_TRANS['No suitable plan']}</li>{/if}
            {if $tpl_errors['depo/depo_frm'][0]=='oper_disabled'}<li>{$_TRANS['Operation disabled']}</li>{/if}
        </ul>
    {/if}

    <form method="post" name="depo/depo_frm">
        <input name="dID" id="depo/depo_frm_dID" value="{$el.dID}" type="hidden">
        <input name="duID" id="depo/depo_frm_duID" value="{$el.duID}" type="hidden">
        <input name="dcCurrID" id="depo/depo_frm_dcID" value="{$el.dcCurrID}" type="hidden">

        <div class="table-responsive">
            <table class="table">
                <tbody>
                
                <tr>
                    <th>{$_TRANS['Status']}</th>
                    <td>{$ststrs[$el.dState]}</td>
                </tr>
                <tr>
                    <th>{$_TRANS['Created']}</th>
                    <td>{$el.dCTS}</td>
                </tr>
                {if $from_admin}
                    <tr>
                        <th>{$_TRANS['User']}</th>
                        <td>
                            <a href="{_link module='account/admin/user'}?id={$el.uID}">
                                {$el.uLogin}
                            </a>
                        </td>
                    </tr>
                {/if}
                {if $from_admin}
                    <div class="block_form_el text_type cfix">
                        {include file='balance/bal.tpl'}
                    </div>
                {/if}
                <tr>
                    <th>{$_TRANS['Curr']}</th>
                    <td>{$el.dcCurrID}</td>
                </tr>
                <tr>
                    <th>{$_TRANS['Amount']}</th>
                    <td>{_z($el.dZD, $el.dcCurrID, 1)}</td>
                </tr>
                <tr>
                    <th>{$_TRANS['Plan']}</th>
                    <td>{valueIf($from_admin, "<a href=\"{_link module='depo/admin/plan'}?id={$el.dpID}\" target=\"_blank\">{$el.pName}</a>", $el.pName)}</td>
                </tr>
                <tr>
                    <th>{$_TRANS['Accruals']}</th>
                    <td>{$el.dNPer} {$_TRANS['from']} {$el.pNPer}</td>
                </tr>
                <tr>
                    <th>{$_TRANS['Last accrual']}</th>
                    <td>{$el.dLTS}</td>
                </tr>
                <tr>
                    <th>{$_TRANS['Next accrual']}</th>
                    <td>{$el.dNTS}</td>
                </tr>
                {if !$chg}
                    <tr>
                        <th>{$_TRANS['Compaund']}</th>
                        <td>{$el.dCompnd}</td>
                    </tr>
                {/if}
                {if $chg}
                    <tr>
                        <th>{$_TRANS['Compaund']}</th>
                        <td><input name="Compnd" id="depo/depo_frm_Compnd" value="{if $el.Compaund}{$el.Compaund}{else}{$el.dCompnd}{/if}" type="text" ></td>
                    </tr>
                {/if}
                {if $el.dState <= 1}
                    <tr>
                        <th>{$_TRANS['Premature withdrawal']}</th>
                        <td> {valueIf($el.pClComis >= 100, $_TRANS['Disabled'],  valueIf($el.dNPer >= $el.pMPer, $_TRANS['Enabled'], $_TRANS['Remaining accruals']|cat:($el.pMPer - $el.dNPer)))}</td>
                    </tr>
                {/if}
                </tbody>
            </table>
        </div>
    </form>
</div>

</div>

{/strip}