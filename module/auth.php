<?php

error_reporting(0);
//error_reporting(65535);

$_smode = intval(@$_smode); // show mode: 0-user / 1-ajax / 2-bot (cron, captcha..)
$_auth = intval(@$_auth); // required access level

// Anti-DDoS

//if ($_smode < 2)
//	@include('a-ddos/a-ddos.php');

global $_user, $_currs;
$_user = array(); // guest
$_currs = array();
function _uid()
{
	return intval(@$_SESSION['_uid']);
}

// Connect DB

require_once('module/dbinit.php');

// Load config

foreach ($db->fetchRows($db->select('Cfg')) as $c)
{
	if (substr($c['Prop'], 0, 1) == '_') // array
		$c['Val'] = asArray($c['Val'], HS2_NL);
	if ($c['Module'])
		$c['Prop'] = $c['Module'] . '_' . $c['Prop'];
	$_cfg[$c['Prop']] = $c['Val'];
}
$_GS['site_name'] = $_cfg['Sys_SiteName'];
if (!$_cfg['UI__Langs'])
	$_cfg['UI__Langs'] = array($_GS['default_lang']);

require_once('module/lib.php');

if ($_smode < 2) // user mode
{
	session_start(); // ??? deadlock

	if (($_cfg['Sec_HTTPSMode'] == 1) and !$_GS['https'])
		goToURL(fullURL('*', true));

	$login_link = moduleToLink('account/login');
	if ($_smode < 1)
	{
		if (!isset($_SESSION['_uid'])) // first time or new session
		{
			$_SESSION['_sid'] = $_cfg['sys_id'];
			$_SESSION['new_session'] = true;
			$_SESSION['_uid'] = 0;
			$_SESSION['show_intro'] = true;
			if ($sess = _COOKIE('sess'))
			{
				if ($uid = $db->fetch1($db->select('Users', 'uID', 'uLSess=?', array($sess))))
				{
					useLib('account/login');
					opLogin($uid, fullURL('*'), false, false, true);
				}
				setcookie('sess', '', time() - HS2_UNIX_DAY, '/');
			}
		}
		else
		{
			if ($_SESSION['_sid'] != $_cfg['sys_id']) // wrong systemID
			{
				session_destroy();
				goToURL();
			}
			unset($_SESSION['new_session']);
		}

		// Language (Interface)

		$_SESSION['_lang'] = @reset($_cfg['UI__Langs']); // default primary
		$a = explode(',', str_replace(';', ',', $_SERVER['HTTP_ACCEPT_LANGUAGE'])); // detect from browser
		if (_COOKIE('lang'))
			array_unshift($a, _COOKIE('lang'));
		foreach ($a as $s)
			if (@in_array($l = Get1ElemL($s, '-'), $_cfg['UI__Langs']))
			{
				$_SESSION['_lang'] = $l;
				break;
			}

		// Reflink

		if ($_cfg['Ref_Word'] and !_SESSION('_ref'))
			if ($ref = exValue(_GET($_cfg['Ref_Word']), _COOKIE('ref')))
				if ($db->count('Users', 'uLogin=? and uState=1', array($ref))) // only active inviter
				{
					setcookie('ref', $ref, time() + 14 * HS2_UNIX_DAY, '/');
					$_SESSION['_ref'] = $ref;
				}

		if (($_GS['module'] != 'account/login') and !_uid() and ($_auth > 0)) // auth required!
			goToURL($login_link . '?url=' . urlencode($_GS['uri']));

		if (!$_cfg['Cron_ByHost'] and $_GS['is_local'])
			@include_once('cron.php');
	}
	list($h, $m) = explode(':', $_cfg['UI_DefaultTZ'], 2);
	$_GS['TZ'] = $h * HS2_UNIX_HOUR + $m * HS2_UNIX_MINUTE;

	if (_uid()) // already logged
	{
		$lt = _SESSION('_lts'); // last time
		$_SESSION['_lts'] = time();
		if (isset($_COOKIE['tz'])) // auto timezone
		{
			$db->update('AddInfo', array('aTZ' => -_COOKIEN('tz')), '', 'auID=?d', array(_uid()));
			setcookie('tz', '', time() - HS2_UNIX_DAY, '/');
		}
		$_user = $db->fetch1Row($db->select('Users LEFT JOIN AddInfo ON auID=uID', '*', 'uID=?d', array(_uid())));
		if (subStamps($_user['uLTS']) > HS2_UNIX_MINUTE)
			$db->update('Users', array('uLTS' => timeToStamp()), '', 'uID=?d', array(_uid()));
		if ($_GS['module'] != 'account/login')
		{
			if (!$_user)
				goToURL($login_link . '?out=not_found');
			if ($_user['aSessIP'] and (_SESSION('_lip') != $_GS['client_ip']))  // !!!IP changed!!!
				goToURL($login_link . '?out=ip_changed');
			if ($_user['aSessUniq'] and (_SESSION('_lsess') != $_user['uLSess'])) // !!!session changed!!!
				goToURL($login_link . '?out=session_changed');
			$t = exValue($_cfg['Sec_TimeOut'], $_user['aTimeOut']);
			if (!$_GS['is_local'] and ($t > 0)) // !!!auto logout (sess exp)!!!
			{
				$t = $lt + ($t * HS2_UNIX_MINUTE) - time();
				if ($t <= 0)
					goToURL($login_link . '?out=time_out');
			}
			if (($_cfg['Sec_PassTime'] > 0) and (subStamps($_user['uPTS']) > (0 + $_cfg['Sec_PassTime']) * HS2_UNIX_DAY))
			{
				if ($_GS['module'] != 'account/change_pass')
					goToURL(moduleToLink('account/change_pass') . '?need_change');
			}
			elseif ($_cfg['Sec_ForceReConfig'] and $_user['aNeedReConfig'])
			{
				if ($_GS['module'] != 'account')
					goToURL(moduleToLink('account'));
			}
			elseif ($_cfg['Sys_NeedReConfig'] and ($_user['uLevel'] >= 90))
			{
				if ($_GS['module'] != 'system/admin/setup_main')
					goToURL(moduleToLink('system/admin/setup_main'));
			}
		}

		$_SESSION['_lang'] = $_user['uLang'];
		if ($_auth >= 90)
		{
			if ($_cfg['Sec__IPs'])
				if (!in_array($_GS['client_ip'], $_cfg['Sec__IPs']))
					showInfo('*Denied', $login_link); // !!!Access denied!!!
			$_GS['TZ'] = 0; // In Admin panel time zone = 0!!!
		}
		else
			$_GS['TZ'] = $_user['aTZ'] * HS2_UNIX_MINUTE;
	}
	if ($_auth > $_user['uLevel'])
		showInfo('*Denied', $login_link); // !!!Access denied!!!
	if ($_cfg['Sys_LockSite'] and ($_user['uLevel'] < 90) and ($_GS['module'] != 'account/login'))
		goToURL($login_link . valueIf(_uid(), '?out=site_locked'));
	setPage('user', $_user);

	$_currs = array();
	foreach (array('USD', 'EUR', 'RUB', 'BTC') as $cn)
		foreach (array('Bal', 'Lock', 'Out') as $p)
//			if (($z = $_user["u$p$cn"]) != 0)
				$_currs[$cn][$p] = $_user["u$p$cn"];
	setPage('currs', $_currs);
 	$_currs2 = $db->fetchIDRows($db->select('Currs LEFT JOIN Wallets ON wcID=cID and wuID=?d','*', 'cDisabled=0', array(_uid()), 'cID'), false, 'cID');

	// Main vars

	$_GS['lang'] = getLang($_SESSION['_lang']); // lang
	$_GS['lang_dir'] = 'tpl/'; // tpl lang dir

    $_TRANS = require_once('lib/trans/'.$_GS['lang'].'.php');

    setPage('_TRANS', $_TRANS, 0);

	setPage('_auth', $_auth);
	setPage('_cfg', $_cfg);
	setPage('root_url', $_GS['root_url']);
	setPage('current_lang', $_GS['lang']);
	setPage('img_path', 'images/' . $_GS['lang'] . '/');
	setPage('css_path', 'css/' . $_GS['lang'] . '/');

	@include_once($_GS['module_dir'] . 'udf.php');

	if ($_auth >= 90)
		$_onstart['admin'] = 90;

	// onStart init

	foreach ($_onstart as $m => $l)
		if ($_auth >= $l)
			if (file_exists($f = $_GS['module_dir'] . $m . '/onstart.php'))
				@include_once($f);

	if ($_smode < 1)
	{
		updateUserCounters();

		// Intro

		if ($_cfg['UI_ShowIntro'] and ($_GS['module'] == 'index') and !get1ElemL($_GS['uri'], '?'))
			if (($_cfg['UI_ShowIntro'] == 2) or _SESSION('show_intro'))
				if ($i = moduleToLink('udp/intro'))
				{
					unset($_SESSION['show_intro']);
					goToURL($i);
				}
	}

}
else // non-user mode
{
	$_currs = array();
	foreach (array('USD', 'EUR', 'RUB', 'BTC') as $cn)
		foreach (array('Bal', 'Lock', 'Out') as $p)
			$_currs[$cn][$p] = 0;
	$_currs2 = $db->fetchIDRows($db->select('Currs', '*', 'cDisabled=0', null, 'cID'), false, 'cID');
}

function getStats() {
	global $db;

	$query = "SELECT oSum, ocCurrID FROM Opers WHERE oState = 3 AND oOper = 'CASHIN' AND ouID = " . _uid();

	$array = $db->fetchRows($db->query($query));

	foreach ($array as $key => $value) {
		$balance['cashin'][$value['ocCurrID']] += $value['oSum'];
	}

	$query = "SELECT oSum, ocCurrID FROM Opers WHERE oState = 3 AND oOper = 'CALCIN' AND ouID = " . _uid();

	$array = $db->fetchRows($db->query($query));

	foreach ($array as $key => $value) {
		$balance['depo'][$value['ocCurrID']] += $value['oSum'];
	}

	$query = "SELECT oSum, ocCurrID FROM Opers WHERE oState = 3 AND oOper = 'REF' AND ouID = " . _uid();

	$array = $db->fetchRows($db->query($query));

	foreach ($array as $key => $value) {
		$balance['ref'][$value['ocCurrID']] += $value['oSum'];
	}

	$balance['contribution'] = getContribution();

	$query = "SELECT oSum, ocCurrID FROM Opers WHERE oOper = 'CASHOUT' AND oState = 3 AND ouID = " . _uid();

	$array = $db->fetchRows($db->query($query));

	foreach ($array as $key => $value) {
		$balance['cashout'][$value['ocCurrID']] += $value['oSum'];
	}

	$balance['depo_sum'] = getTurnover();

	return $balance;
}

if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
    setPage('stats', []);
} else {
	setPage('stats', getStats());
}

function getContribution() {
	global $db;

	$query = "SELECT dcCurrID, dZ0 FROM Deps WHERE dState = 1 AND duID = " . _uid();

	$array = $db->fetchRows($db->query($query));

	foreach ($array as $key => $value) {
		$balance[$value['dcCurrID']] += $value['dZ0'];
	}

	return $balance;
}

function getTurnover() {
	global $db;

	$query = "SELECT * FROM Cfg";

	$cfg = $db->fetchRows($db->query($query));
	$result = array_filter($cfg, function ($key) {
		if ($key['Prop'] == 'Levels') {
			return true;
		}
	});

	$cfg_array = array_values($result);

	$ref_levels = $cfg_array[0]['Val'];

	$refs = array();
	$a = array(_uid());
	$n = $ref_levels;
	if ($n < 1)
		$n = 1;
	for ($i = 0; $i < $n; $i++) {
		$refs[$i]['users'] = $db->fetchIDRows($db->select(
		'Users U LEFT JOIN AddInfo ON auID=uID LEFT JOIN Opers_refs_summ AS t1 ON t1.User_ID=U.uID AND t1.Ref_ID=\''.mysql_escape_string(_uid()).'\'', 
			'U.uID, U.uSumDepoUSD, U.uSumDepoBTC, 
			U.uSumDepo AS ZDepo,
			IF(t1.Sum IS NULL, 0, t1.Sum) AS ZRef',
			'U.uRef ?i', array($a), 'U.uLogin'), false, 'uID');
		$a = array_keys($refs[$i]['users']);
	}

	$depo_sum = array();

	$percent = [1, 0.7, 0.5, 0.25, 0.1];

	foreach ($refs as $key => $value) {
		foreach ($value['users'] as $user) {
			$depo_sum['USD'] += $user['uSumDepoUSD'] * $percent[$key];
			$depo_sum['BTC'] += $user['uSumDepoBTC'] * $percent[$key];
		}
	}

	return $depo_sum;
}

function check_turnover() {
	global $db;

	$query = "SELECT turnover_id FROM Users WHERE uID = " . _uid();
	$result = $db->query($query);

	$turnover_id = $db->fetch1Row($result);
	$profiles = get_turnover_profiles();

	$depos = getContribution()['USD'] + getContribution()['BTC'] * 2500;
	$turnover = getTurnover()['USD'] + getTurnover()['BTC'] * 2500;

	if($turnover_id && $turnover_id['turnover_id']) {
		$career = '';

		$last_career = get_turnover_profile($turnover_id['turnover_id']);

		foreach ($profiles as $key => $profile) {
			if ($depos >= $profile['cashin'] && $turnover >= $profile['amount']) {
				$career = $profile;
			}
		}

		if($career['id'] > $last_career['id']) {
			setTurnoverProfile($career['id']);
			creditAward($career['award']);
		}
	} else {
		$career = '';

		foreach ($profiles as $key => $profile) {
			if ($depos >= $profile['cashin'] && $turnover >= $profile['amount']) {
				$career = $profile;
			}
		}

		if($career['id']) {
			setTurnoverProfile($career['id']);
		} else {
			setTurnoverProfile(1);
		}

		
		redirect('/career');
	}

	$next_profile = $profiles[$career['id']];

	$result_percentage = getPercentage($depos, $turnover, $next_profile);

	setPage('result_percentage', $result_percentage);
}

function getPercentage($career_deposit, $career_turnover, $profile) {

	$turnover_percent = $career_turnover * 100 / $profile['amount'];
	$deposit_percent = $career_deposit * 100 / $profile['cashin'];

	if ($deposit_percent > 100) {
		$deposit_percent = 100;
	} elseif ($turnover_percent > 100) {
		$turnover_percent = 100;
	}

	$deposit_percent = $deposit_percent * 0.5;
	$turnover_percent = $turnover_percent * 1.5;

	$percentage = ($turnover_percent + $deposit_percent) / 2;

	return $percentage;
}

function creditAward($amount) {

	$amount = trim(htmlspecialchars($amount));

	global $db;

	$query = "SELECT uBalUSD FROM Users WHERE uID = " . _uid();
	$result = $db->query($query);

	$user_balance = $db->fetch1Row($result)['uBalUSD'];

	$active_balance = $user_balance + $amount;

	$query = "UPDATE Users SET uBalUSD = " . htmlspecialchars($active_balance) . " WHERE uID = " . _uid();
	$db->query($query);

	$time = timeToStamp(time());
	$memo = 'Reward for Career';
	$id = _uid();

	$query = "INSERT INTO Opers (oCTS, oATS, ouID, oOper, ocID, ocCurrID, ocCurrID2, oSum, oSum2, oSumReal, oTS, oState, oPTS, oMemo, oNTS) 
	VALUES ('$time', '$time', '$id' , 'BONUS', '4', 'USD', 'USD', '$amount', '$amount', '$amount', '$time', '3', '0', '$memo', '$time')";

	$db->query($query);

	redirect('/career');
}

function get_turnover_profile($id) {
	global $db;

	$query = "SELECT * FROM Career WHERE id = " . htmlspecialchars($id);
	$result = $db->query($query);

	$member = $db->fetch1Row($result);

	return $member;
}

function get_turnover_profiles() {
	global $db;

	$query = "SELECT * FROM Career";
	$result = $db->query($query);

	$member = $db->fetchRows($result);

	return $member;
}

function setTurnoverProfile($id) {
	global $db;

	$query = "UPDATE Users SET turnover_id = " . htmlspecialchars($id) . " WHERE uID = " . _uid();
	$db->query($query);
}


// Get user data
function get_user_info()
{
	global $db;

	$query = "SELECT * FROM AddInfo WHERE auID = " . _uid();

	$result = $db->query($query);

	$user = $db->fetch1Row($result);

	setPage('user_profile', $user);
}

function getUserProfile($field = '') {
	global $db;

	if ($field) {
		$field = implode(", ", $field);

		$query = "SELECT " . htmlspecialchars($field) . " FROM Users WHERE uID = " . _uid();
	} else {
		$query = "SELECT * FROM Users WHERE uID = " . _uid();
	}
	
	$result = $db->query($query);

	$user = $db->fetch1Row($result);

	return $user;
}

get_user_info();

function dd($value)
{

	echo "<pre>";

	print_r($value);

	echo "</pre>";

	die;
}

function redirect($url) {

	$url = trim($url);

	header('Location: ' . $url);
}

setPage('refurl', 'https://alt-trade.com/?' . $_cfg['Ref_Word'] . '=' . $_user['uLogin']);