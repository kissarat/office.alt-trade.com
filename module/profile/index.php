<?php

require_once('module/auth.php');

require 'vendor/autoload.php';

// import the Intervention Image Manager Class
use Intervention\Image\ImageManagerStatic as Image;


if ($_FILES['logo']) {

	try {
    
	    // Undefined | Multiple Files | $_FILES Corruption Attack
	    // If this request falls under any of them, treat it invalid.
	    if (
	        !isset($_FILES['logo']['error']) ||
	        is_array($_FILES['logo']['error'])
	    ) {
	        throw new RuntimeException('Invalid parameters.');
	    }

	    // Check $_FILES['logo']['error'] value.
	    switch ($_FILES['logo']['error']) {
	        case UPLOAD_ERR_OK:
	            break;
	        case UPLOAD_ERR_NO_FILE:
	            throw new RuntimeException('No file sent.');
	        case UPLOAD_ERR_INI_SIZE:
	        case UPLOAD_ERR_FORM_SIZE:
	            throw new RuntimeException('Exceeded filesize limit.');
	        default:
	            throw new RuntimeException('Unknown errors.');
	    }

	    // You should also check filesize here. 
	    if ($_FILES['logo']['size'] > 10000000) {
	        throw new RuntimeException('Exceeded filesize limit.');
	    }

	    // DO NOT TRUST $_FILES['logo']['mime'] VALUE !!
	    // Check MIME Type by yourself.
	    $finfo = new finfo(FILEINFO_MIME_TYPE);
	    if (false === $ext = array_search(
	        $finfo->file($_FILES['logo']['tmp_name']),
	        array(
	            'jpg' => 'image/jpeg',
	            'png' => 'image/png',
	            'gif' => 'image/gif',
	        ),
	        true
	    )) {
	        throw new RuntimeException('Invalid file format.');
	    }


	    $file_name = sha1_file($_FILES['logo']['tmp_name']) . '.' . $ext;
	    $file_path = './upload/avatar/' . $file_name;

	    upload_image($file_name, $file_path);

	    setPage('success', $file_name);

	} catch (RuntimeException $e) {

	    setPage('error', $e->getMessage());

	}
}

if(isset($_POST['profile'])) {
	$name = htmlspecialchars(trim($_POST['name']));

	save($name);
}

function save($name)
{
	global $db;

	$query = "UPDATE AddInfo SET aName = '" . mysql_real_escape_string($name) . "' WHERE auID = " . _uid();

	$db->query($query);

	header('Location: /profile');
}

function upload_image($name, $file_path)
{
	// read image from temporary file
	$img = Image::make($_FILES['logo']['tmp_name']);

	$img->fit(200, 200);
	$img->save($file_path);

	global $db;

	$query = "UPDATE AddInfo SET aAvatar = '" . mysql_real_escape_string($name) . "' WHERE auID = " . _uid();

	$db->query($query);

	redirect('/profile');
}

showPage();