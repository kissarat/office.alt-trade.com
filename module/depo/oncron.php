<?php

if ($_cfg['Depo_ChargeMode'] == 1)
{
	useLib('depo');
	$dl = $db->select('Deps LEFT JOIN Plans ON pID=dpID LEFT JOIN Users ON uID=duID', '*', 
		'dState=1 and (dNPer<pNPer or pNPer=0) and dNTS<=? and dLTS<=?', array(timeToStamp(), timeToStamp(time() - 1 * HS2_UNIX_MINUTE)), 'dLTS, dID');
	$t = time();	
	while ((abs(time() - $t) < 20) and ($d = $db->fetch($dl)))
		opDepoCharge($d);
}

if ($_cfg['Depo_ShowStat'])
{
	useLib('depo');
	@file_put_contents('tpl_c/stat.dat', @serialize(depoGetStat()));
}
	
?>