<?php

require_once('module/auth.php');


if($_GET['type'] == 'month') {
	echo getProfit('month');
} else if($_GET['type'] == 'perc') {
	echo json_encode(getPercentStats());
} else if($_GET['type'] == 'day') {
	$profit = getProfit('day');
	echo $profit;
}

if($_GET['stats'] == 'percent') {
	echo $_GET['callback'] . '(' . json_encode(getPercentStats()) . ')';
}

if($_GET['stats'] == 'month_percent') {
	echo $_GET['callback'] . '(' . json_encode(getPercentByMonth($_GET['month'])) . ')';
}

if($_GET['stats'] == 'amount_deps') {
	echo $_GET['callback'] . '(' . json_encode(getAmountDepos()) . ')';
}

function getPercentStats()
{
	global $db;

	$query = 'SELECT cTS, cPerc FROM Calend WHERE cType = 1';
	$percents = $db->fetchRows($db->query($query));

	$planPerc = getPlanPerc();

	foreach ($percents as $key => $perc) {
		$time = stampToTime($perc['cTS']);

		$day = date('j', $time);
		$month = date('n', $time);
		$year = date("Y", $time);

		$statsByDay[$year][$month][$day] = (float)$perc['cPerc'];
	}

	$thisYear = date("Y");
	$thisMonth = date('n');

	$stats = $statsByDay[$thisYear][$thisMonth];

	for ($i=1; $i <= date('t'); $i++) {
		if($i > date('j')) {
			$statsByMonth[$i] = '';
		} else {
			if(array_key_exists($i, $stats)) {
				$statsByMonth[$i] = $stats[$i];
			} else {
				$statsByMonth[$i] = $planPerc;
			}
		}
	}

	return $statsByMonth;
}

function getPercentByMonth($by_month)
{
	global $db;

	if (empty($by_month)) {
		$by_month = date('n');
	}

	$query = 'SELECT cTS, cPerc FROM Calend WHERE cType = 1';
	$percents = $db->fetchRows($db->query($query));

	foreach ($percents as $key => $perc) {
		$time = stampToTime($perc['cTS']);

		$day = date('j', $time);
		$month = date('n', $time);
		$year = date("Y", $time);

		$statsByDay[$year][$month][$day] = $perc['cPerc'];
	}

	$thisYear = date("Y");

	$stats = $statsByDay[$thisYear][$by_month];

	$statsByMonth = 0;

	
	if($by_month == date('n')) {
		$count = date('j');
	} else {
		$count = count($stats);
	}

	$stats = array_values($stats);

	for ($i=0; $i < $count; $i++) {
		$statsByMonth += $stats[$i];
	}

	return $statsByMonth;
}

function getPlanPerc()
{
	global $db;

	$query = 'SELECT pPerc FROM Plans';
	$result = $db->fetchRows($db->query($query));

	return (float)$result[0]['pPerc'];
}

function getProfit($period) {
	global $db;

	$query = 'SELECT oOper, oSum, oTS FROM Opers WHERE oState = 3 AND ocCurrID = "USD" AND ouID = ' . _uid();

	$stats = $db->fetchRows($db->query($query));

	foreach ($stats as $key => $oper) {
		if($oper['oOper'] == 'REF' || $oper['oOper'] == 'CALCIN') {
			$time = stampToTime($oper['oTS']);

			$day = date('j', $time);
			$month = date('n', $time);
			$year = date("Y", $time);

			$statsByDate[$year][$month][$day] += $oper['oSum'];
			$statsByMonth[$year][$month] += $oper['oSum'];
		}
	}

	$thisYear = date("Y");
	$thisMonth = date('n');

	$thisMonthStatsByDate = $statsByDate[$thisYear][$thisMonth];

	for ($i=1; $i <= date('t'); $i++) {
		if($i > date('j')) {
			$data[$i] = '';
		} else {
			if(array_key_exists($i, $thisMonthStatsByDate)) {
				$data[$i] = number_format($thisMonthStatsByDate[$i], 1);
			} else {
				$data[$i] = '';
			}
		}
	}

	$thisYearStats = $statsByMonth[$thisYear];

	for ($i=1; $i <= 12; $i++) {
		if($i > date('n')) {
			$yearStats[$i] = '';
		} else {
			if (array_key_exists($i, $thisYearStats)) {
				$yearStats[$i] = (int)$thisYearStats[$i];
			} else {
				$yearStats[$i] = '';
			}
		}
		
	}

	if($period == 'month') {
		return json_encode($yearStats);
	} else {
		return json_encode($data);
	}
}

function getAmountDepos() {
	global $db;

	$query = 'SELECT COUNT(dID) AS count FROM Deps';

	$result = $db->fetchRows($db->query($query));

	return $result[0]['count'];
}