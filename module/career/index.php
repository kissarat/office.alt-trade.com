<?php

require_once('module/auth.php');

check_turnover();

$profiles = get_turnover_profiles();

$user = getUserProfile(['turnover_id']);

setPage('user_career_id', $user['turnover_id']);
setPage('profiles', $profiles);

showPage();